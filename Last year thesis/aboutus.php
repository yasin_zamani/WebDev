<!--
 Author : Yasin Zamani Konari
 email: yzk2@protonmail.com
 -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="About us Page">
    <meta name="author" content="Yasin Zamani Konari">

    <title>CLASSIC VENUE</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="shared/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
    <div class="header">
        <nav class="navbar navbar-expand-sm">
            <div class="container justify-content-end">
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse"
                        data-bs-target="#CollapseContent">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="CollapseContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0 nav-pills">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="aboutus.php">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="venue.php">Venue</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contactus.php">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signup.php">Register</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signin.php">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="container">
    <br>
    <h3 class="text-muted">CLASSIC VENUE</h3>
    <br>
</div>

<div class="container text-center">

    <div class="row">
        <div class="col">
            <h2 class="text-center">About Us</h2>
            <p class="text-center lead">We established CLASSIC VENUE in 2012, and now the company is under new
                management.Our team will help you with your bookings or other arrangements in our venue?

                All of us are professionals with different backgrounds and cultures, With this expertise we will help
                you customise the venue as you wish.Our team is always willing to help you and can provide you with the
                best experience in customer service.It awarded us for one of the best and cheap venue in our region. We
                are always maintaining our bookings and management systems.We will give our best for you to enjoy your
                stay and have a great time with us. Below you can contact us for further enquiries.</p>
            <a class="btn bg-success" href="contactus.php">Contact Us</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <h2 class="text-center">Our Story</h2>
            <p class="lead text-justify">The CEO Mirwais Samani established CLASSIC VENUE in 2012, since then the
                amazing team of CLASSIC VENUE are accountable for holding weddings, birthday parties or custom requests
                per contact. <br>Our team will always make sure that our venues are affordable. Classic Venue always
                kept the prices of the venue low and were listening to the community feedbacks, with these feedbacks it
                made our company to grow in an Exponential Growth, we are hoping for it to continue like this. We are
                thankful for public to offer us feedbacks. </p>
        </div>
    </div>
</div>
<div class="container">
    <footer class="footer">
        <p>© CLASSIC VENUE 2021</p>
    </footer>

</div>

</body>
</html>
