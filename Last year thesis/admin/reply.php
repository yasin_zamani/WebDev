<?php
/* Replys to customer
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
include "../functions.php";
$id = $_POST['id'];
$msg = $_POST['message'];
//var_dump($_POST);
$conn = connection();
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$stmt = $conn->prepare("SELECT * FROM contact WHERE id = ?");
$stmt->bind_param('i', $id);
$stmt->execute();
$result = $stmt->get_result();
$row = $result->fetch_assoc();
if ($row) {
    $status = "1";
    $stmt1 = $conn->prepare("UPDATE contact SET status=? WHERE id= ?");
    $stmt1->bind_param('ii', $status, $id);
    $stmt1->execute();
    reply($row['email'], $msg);
}

