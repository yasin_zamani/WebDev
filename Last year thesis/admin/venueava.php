<!--
 Author : Yasin Zamani Konari
 email: yzk2@protonmail.com
 -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta name="description" content="View Venue">
    <meta name="author" content="Yasin Zamani Konari">

    <title>CLASSIC VENUE</title>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <script src="../js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="../shared/js/scripts.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
          integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
          crossorigin="anonymous"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
            integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
            crossorigin="anonymous"></script>


</head>

<body>

<?php
session_start();

if ($_SESSION['type'] == 'admin') {

} else {
    echo 'session not set :(!)';
    header("location:../signin.php?error=invalidSession");
}


?>


<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="list-group">
        <br>
        <a href="dashboard.php" class="list-group-item list-group-item-action">Book Venue</a>
        <a href="venueava.php" class="list-group-item list-group-item-action active">Venue Availability</a>
        <a href="viewreservations.php" class="list-group-item list-group-item-action">Manage Reservations</a>
        <a href="upcomingevents.php" class="list-group-item list-group-item-action">Upcoming Events</a>
        <a href="bookingreq.php" class="list-group-item list-group-item-action">Check Requests</a>
        <a href="addassistant.php" class="list-group-item list-group-item-action">Add Assistant</a>
        <a href="deleteassistant.php" class="list-group-item list-group-item-action">Delete Assistant</a>
        <a href="../shared/logout.php" class="list-group-item list-group-item-action">Log out</a>
    </div>

    <!-- Page Content -->

    <div class="table-responsive container mt-10 mx-auto">
        <input id="search-input" class="form-control" type="text" placeholder="Search for date format: YYYY-MM-DD">
        <div class="row container-fixed">
            <div class="col-md-50 mx-auto">
                <table class="table bg-white rounded border table-striped table-grey table-hover container-fixed">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col"> Date</th>
                        <th scope="col"> Name</th>
                        <th scope="col"> Email</th>
                        <th scope="col"> Contact</th>
                        <th scope="col"> Confirmation</th>
                        <th scope="col"> Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    include '../functions.php';
                    $paid = 'paid';
                    $conn = connection();
                    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
                    $query = "select reservation_id,dato,firstname,email,contact,confirmation,status from reservation where status = ? order by dato;";
                    $stmt = $conn->prepare($query);
                    $stmt->bind_param('s', $paid);
                    $stmt->execute();
                    $result = $stmt->get_result();
                    $counter = 0;
                    while ($row = $result->fetch_assoc()) {
                        $counter++;
                        echo '
                <tr class="record" id=' . $row['reservation_id'] . '>
                <th scope="row">' . $counter . '</th>
                <td row="dato">' . $row['dato'] . '</td>
                <td row="name">' . $row['firstname'] . '</td>
                <td row="email">' . $row['email'] . '</td>
                <td row="contact">' . $row['contact'] . '</td>
                <td row="confirmation">' . $row['confirmation'] . '</td>
                <td>' . $row['status'] . '</td>
                </tr>
                ';

                    }
                    ?>

                    </tbody>
                </table>

            </div>
        </div>
</body>
</html>

