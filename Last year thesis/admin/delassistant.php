<?php
/* deletes assistant
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
include "../connection.php";
session_start();
switch ($_SESSION['type']) {
    case 'admin':
        $conn = connection();
        $confirmation = $_GET['delete'];
        $sql = "DELETE from users WHERE id = ?;";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $confirmation);
        $stmt->execute();
        break;
    default:
        echo 'invalid session';
        break;
}
