<!--
 Author : Yasin Zamani Konari
 email: yzk2@protonmail.com
 -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta name="description" content="View Reservation">
    <meta name="author" content="Yasin Zamani Konari">

    <title>CLASSIC VENUE</title>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <script src="../js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="../shared/js/scripts.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
          integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
          crossorigin="anonymous"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
            integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
            crossorigin="anonymous"></script>

</head>

<body>

<?php
session_start();

if ($_SESSION['type'] == 'admin') {
} else {
    echo 'session not set :(!)';
    header("location:../signin.php?error=invalidSession");
}


?>


<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="list-group">
        <br>
        <a href="dashboard.php" class="list-group-item list-group-item-action">Book Venue</a>
        <a href="venueava.php" class="list-group-item list-group-item-action">Venue Availability</a>
        <a href="viewreservations.php" class="list-group-item list-group-item-action active">Manage Reservations</a>
        <a href="upcomingevents.php" class="list-group-item list-group-item-action">Upcoming Events</a>
        <a href="bookingreq.php" class="list-group-item list-group-item-action">Check Requests</a>
        <a href="addassistant.php" class="list-group-item list-group-item-action">Add Assistant</a>
        <a href="deleteassistant.php" class="list-group-item list-group-item-action">Delete Assistant</a>
        <a href="../shared/logout.php" class="list-group-item list-group-item-action">Log out</a>
    </div>

    <!-- Page Content -->

    <div class="table-responsive container mt-10 mx-auto">
        <h3>Current Table Displays all reservations with search functionality</h3>
        <input id="search-input" class="form-control" type="text" placeholder="Search Confirmation">
        <div class="row container-fixed">
            <div class="col-md-50 mx-auto">
                <table class="table bg-white rounded border table-striped table-grey table-hover container-fixed">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col"> Confirmation</th>
                        <th scope="col"> Name</th>
                        <th scope="col"> Email</th>
                        <th scope="col"> Contact</th>
                        <th scope="col"> Date</th>
                        <th scope="col"> Status</th>
                        <th scope="col"> Guests</th>
                        <th scope="col"> Message</th>
                        <th scope="col"> Bundle</th>
                        <th scope="col"> Total</th>
                        <th scope="col"> Edit</th>
                        <th scope="col"> Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    include '../functions.php';
                    $conn = connection();
                    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
                    $query = "SELECT * FROM reservation ORDER BY firstname;";
                    $result = $conn->query($query);
                    $counter = 0;
                    while ($row = $result->fetch_assoc()) {

                        $counter++;
                        echo '
                <tr class="record" id=' . $row['reservation_id'] . '>
                <th scope="row">' . $counter . '</th>
                <td row="key">' . $row['confirmation'] . '</td>
                <td row="name">' . $row['firstname'] . '</td>
                <td row="email">' . $row['email'] . '</td>
                <td row="contact">' . $row['contact'] . '</td>
                <td row="dato">' . $row['dato'] . '</td>
                <td>' . $row['status'] . '</td>
                <td row="guests">' . $row['guests'] . '</td>
                <td style="word-wrap: break-word;min-width: 260px;max-width: 260px;"row="message">' . $row['message'] . '</td>
                <td row=package>' . $row['package'] . '</td>
                <td>' . $row['total'] . '</td>
                <td><a class="btn btn-secondary btn-sm edit" href="#" update="update" data-id="' . $row['reservation_id'] . '">Edit</a></td>
                <td><a class="btn btn-primary btn-sm delbutton" href="#" delete="' . $row['confirmation'] . '" class="delbutton">Delete</a></td>

                </tr>
                ';

                    }
                    ?>
                    <!-- Trigger the modal with a button -->
                    <button type="button" hidden class="btn btn-info btn-lg" data-toggle="modal" row="#myModal">Open
                        Modal
                    </button>
                    <!-- Modal from w3 -->
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <button type="button" class="close d-flex justify-content-end" data-dismiss="modal">
                                    &times;
                                </button>
                                <div class="modal-header">
                                    <h4 class="modal-title">Booking Information</h4>
                                </div>
                                <div class="modal-body ">
                                    <div class="row container-fluid">
                                        <label for="name" class="form-label">Customer Name</label>
                                        <input type="text" id="name" name="visitor_name" placeholder="John Doe"
                                               required>
                                        <label for="email" class="form-label">Customer mail</label>
                                        <input type="email" id="email" name="visitor_email"
                                               placeholder="john.doe@email.com" required>
                                        <label hidden for="id">Customer id</label>
                                        <input hidden type="email" id="secretkey">

                                        <label for="phone" class="form-label">Customer Phone</label>
                                        <input type="tel" id="phone" name="visitor_phone" placeholder="07xxxxxx"
                                               required>

                                        <label for="guests" class="form-label">Guests</label>
                                        <input type="number" id="guests" name="total_guests" placeholder="Number"
                                               min="1" max="5" required>

                                        <label for="reservation_date" class="form-label">Reservation Date</label>
                                        <input id="dataoKontroll" max="2021-12-31" name="date" required>

                                        <label for="package-selection" class="form-label">Package</label>
                                        <select id="package-selection" name="package_preference" required>
                                            <option value="">Option for bundle</option>
                                            <option value="meal1">Meal Option 1</option>
                                            <option value="meal2">Meal Option 2</option>
                                            <option value="nobundle">No bundle</option>
                                        </select>
                                        <hr>
                                        <label for="message">Extra information</label>
                                        <label hidden id="test1">Extra information</label>
                                        <textarea id="message" rows="4" cols="50" name="visitor_message"
                                                  placeholder="Important Detail or Message" required></textarea>
                                        <div class="modal-footer">
                                            <a href="#" class="accept btn btn-default btn-success">Update</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

