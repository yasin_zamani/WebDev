<!--
 Author : Yasin Zamani Konari
 email: yzk2@protonmail.com
 -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta name="description" content="Add Assistants">
    <meta name="author" content="Yasin Zamani Konari">

    <title>CLASSIC VENUE</title>
    <link href="../css/style.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="../js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="../shared/js/scripts.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
          integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
          crossorigin="anonymous"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
            integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
            crossorigin="anonymous"></script>

</head>

<body>

<?php
session_start();

if ($_SESSION['type'] == 'admin') {
} else {
    echo 'session not set :(!)';
    header("location:../signin.php?error=invalidSession");
}


?>


<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="list-group">
        <br>
        <a href="dashboard.php" class="list-group-item list-group-item-action">Book Venue</a>
        <a href="venueava.php" class="list-group-item list-group-item-action">Venue Availability</a>
        <a href="viewreservations.php" class="list-group-item list-group-item-action">Manage Reservations</a>
        <a href="upcomingevents.php" class="list-group-item list-group-item-action">Upcoming Events</a>
        <a href="bookingreq.php" class="list-group-item list-group-item-action">Check Requests</a>
        <a href="addassistant.php" class="list-group-item list-group-item-action active" click="yes">Add Assistant</a>
        <a href="deleteassistant.php" class="list-group-item list-group-item-action">Delete Assistant</a>
        <a href="../shared/logout.php" class="list-group-item list-group-item-action">Log out</a>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-9">
                <div class="card cardbox">
                    <div class="card-header">This form will add Assistant to the database</div>
                    <div class="card-body">
                        <form action="../signup.inc.php" id="login-nav" method="post">
                            <div class="row">
                                <div class="col-lg">
                                    <label class="form-label">Assistant name</label>
                                    <input type="text" id="reg_username" name="user_name" class="form-control"
                                           value="" placeholder="Assistant Name" pattern="([A-zÀ-ž\s])+" minlength="5"
                                           maxlength="20" required>
                                    <label class="form-label">E-mail Address</label>
                                    <input type="email" id="reg_useremail" name="user_email" class="form-control"
                                           value="" placeholder="Email" required>
                                    <label class="form-label">Password</label>
                                    <input type="password" id="reg_userpassword" name="user_password"
                                           class="form-control" placeholder="Password"
                                           pattern="([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*" minlength="5"
                                           maxlength="20" required>
                                    <label class="form-label">Repeat Password</label>
                                    <input type="password" id="reg_userpassword2" name="user_password2"
                                           class="form-control" placeholder="Repeat Password"
                                           pattern="([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*" minlength="5"
                                           maxlength="20"
                                           oninvalid="setCustomValidity('Password must include Letters and Numbers')"
                                           oninput="setCustomValidity('')" required>
                                    <br>
                                    <input hidden type="text" id="type" name="type" value="assistant" readonly>
                                    <button id="reg_submit" name="submit" value="1" class="btn btn-block btn-primary">
                                        Create Account
                                    </button>
                                    <?php
                                    if (isset($_GET['success'])) {
                                        if ($_GET['success'] == 'yes') {
                                            echo '<p class="bg-success">Successfully added</p>';
                                        }
                                    } elseif (isset($_GET['error'])) {
                                        if ($_GET['error'] == 'pwdmistach') {
                                            echo '<p class="bg-warning">Password 1 must be equal to Password 2</p>';
                                        } elseif ($_GET['error'] == 'emailExists') {
                                            echo '<p class="bg-danger">Email Already Exists</p>';
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

