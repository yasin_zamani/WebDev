## Author

Yasin Zamani Konari
yzk2@protonmail.com

# Venue Booking System

PHP System that takes in booking system with payment gateway.

## Getting Started

Below are the instructions on how to launch this system into your server.
## Prerequisites

XAMPP and MariaDB: https://www.apachefriends.org

The database uses mysql
a sample database have been dumped in the folder called : Database
This can be imported via phpmyadmin or the terminal
Below demonstrates importing the data to register database.
mysql> CREATE DATABASE register;
mysql> use register;
mysql> source LOCATION-OF-FILE/register.sql;

## Installing

put myProject into htdocs or your public_html

## Built With
* [HTML,CSS AND JS]
* [PHP](https://www.php.net) Brain of the system
* [BootStrap 5](https://getbootstrap.com/docs/5.0/getting-started/download/) - The frontend framework
* [BootStrap Datepicker](https://cdnjs.com/libraries/bootstrap-datepicker) - Date picker plugin
* [JQuery](https://jquery.com) - Simplified JS and AJAX requests
* [Confirmation](https://stackoverflow.com/questions/7322689/generating-a-random-numbers-and-letters) - inspired code.
* [Modal](https://www.w3schools.com/bootstrap/bootstrap_modal.asp) - inspired code

## Directories
* Admin : Administrator dashboard * PHP
* att : Assistance Dashboard * PHP
* css : CSS file for the pages and compiled bootstrap https://getbootstrap.com/docs/5.0/getting-started/download/
* fpdf : A php class to generate PFD files with php. http://www.fpdf.org
* img : Images for hero/slideshow and confirmation image.
* js: javascript includes complied version of bootstrap, jquery and my own scripts. https://jquery.com
* shared: PHP Files shared between each Dashboard
* shared>js: Javascript scripts shared between each dashboard, this includes the payment gateway and my own scripts written in jquery https://developer.paypal.com/docs/business/javascript-sdk/javascript-sdk-reference/
* user: PHP Files for User dashboard

# Accounts
yzk@aber.ac.uk:yasin123 logins to the admin dashboard.
demo@account.com:demo123 logins to assistant dashboard.
yzk3@aber.ac.uk:yasin123 logins to user dashboard or register through registration form.

# External includes that are not precompiled:
Datepicker script and styling
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js
