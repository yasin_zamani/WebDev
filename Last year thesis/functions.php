<?php
/*
 * The brain of the system
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
include "connection.php";
include 'fpdf/fpdf.php';

/**
 * @param $user
 * @param $email
 * @param $pwd
 * @param $user_type
 * Creates account to the database
 */
function CreateAccount($user, $email, $pwd, $user_type)
{
    $conn = connection();
    $sql = "INSERT INTO users (username,email,password,user_type) VALUES (?,?,?,?);";
    $hashedPassword = password_hash($pwd, PASSWORD_DEFAULT);
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssss", $user, $email, $hashedPassword, $user_type);
    $stmt->execute();
    if ($user_type == 'user') {
        header("location:signup.php?error=sucess");
    }
}

/**
 * @param $pwd
 * @param $pwd2
 * @return bool
 * checks if pwd and pwd matches
 */
function pwdMatch($pwd, $pwd2)
{
    if ($pwd !== $pwd2) {
        $res = true;
    } else {
        $res = false;

    }
    return $res;
}


/**
 * @param $email
 * @return bool to see if email exists in the database
 */
function emailExists($email)
{
    $conn = connection();
    $sql = "SELECT * FROM users WHERE email = ?;";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $result = $stmt->fetch();
    if ($result == $email) {
        $res = true;
    } else {
        $res = false;
    }
    return $res;
}


/**
 * @param $email
 * @param $pwd
 * logins to the account with proper session
 */
function loginAccount($email, $pwd)
{
    $conn = connection();
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $stmt = $conn->prepare("SELECT * FROM users WHERE email = ?");
    $stmt->bind_param('s', $email);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();
    $HashedPW = $row['password'];
    $hashedPasswordCheck = password_verify($pwd, $HashedPW);
    if ($hashedPasswordCheck) {
        $_SESSION['type'] = $row['user_type'];
        $_SESSION['email'] = $row['email'];
        $_SESSION['name'] = $row['username'];
        if ($_SESSION['type'] == 'admin') {
            header('Location:admin/dashboard.php');
        } elseif ($_SESSION['type'] == 'user') {
            header('Location:user/dashboard.php');
        } elseif ($_SESSION['type'] == 'att') {
            header('Location:att/dashboard.php');
        }

    } else {
        header("location:signin.php?error=invalidinfo");
    }
}

/**
 * @return string of confirmation number
 * https://stackoverflow.com/questions/7322689/generating-a-random-numbers-and-letters
 *
 */
function confirmationNumber()
{
    $characters = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime() * 1000);
    $x = 0;
    $code = '';
    while ($x <= 7) {
        $num = rand() % 33;
        $tempnbr = substr($characters, $num, 1);
        $code = $code . $tempnbr;
        $x++;
    }
    return $code;
}

/**
 * @param $firstname
 * @param $email
 * @param $contact
 * @param $dato
 * @param $confirm
 * @param $guests
 * @param $message
 * @param $package
 * @param $total
 * Adds booking to the database
 */
function AddBooking($firstname, $email, $contact, $dato, $confirm, $guests, $message, $package, $total)
{

    $dir;
    $type = $_SESSION['type'];
    if ($type == 'admin') {
        header("location:../$type/dashboard.php?success=yes");
    } elseif ($type == 'att') {
        header("location:../$type/dashboard.php?success=yes");
    } elseif ($type == 'user') {
        header("location:../$type/dashboard.php?success=yes");
    }
    $conn = connection();

    $sql = "INSERT INTO reservation (firstname,email,contact,dato,confirmation,guests,message,package,total) VALUES (?,?,?,?,?,?,?,?,?);";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssssissi", $firstname, $email, $contact, $dato, $confirm, $guests, $message, $package, $total);
    $stmt->execute();
    $stmt->close();
    sendEmail($email, $confirm, $total);

}


/**
 * @param $confirmation checks if confirmation exists in the database and display paybutton.
 */
function customerInfo($confirmation)
{
    $conn = connection();
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $query = "SELECT * FROM reservation WHERE confirmation =?;";
    $stmt = $conn->prepare($query);
    $stmt->bind_param("s", $confirmation);
    $stmt->execute();
    $result = $stmt->get_result();
    /* fetch associative array */
    $row = $result->fetch_assoc();
    if ($row['confirmation'] === $confirmation) {
        //  echo 'passed first condition';
        if ($row['status'] === 'not paid') {
            //  echo 'passed second condition';
            $_SESSION['confirm'] = $row['confirmation'];
            echo 'Customer Reservation ID:', $row['reservation_id'];
            echo '<br>';
            echo 'Customer Name:', $row['firstname'];
            echo '<br>';
            echo 'Customer Email:', $row['email'];
            echo '<br>';
            echo 'Payment Status:', $row['status'];
            echo '<br>';
            echo 'Customer Confirmation Number:', $row['confirmation'];
            echo '<br>';
            echo '<p id="amount" hidden>' . $row['total'] . '</p>';
            echo '<h1 class="text-center">Payment GateWay Paypal!</h1>';
            echo '<div  class="d-flex justify-content-center" id="paypal-payment-button">';
        } else if ($row['status'] === 'paid') {
            echo 'Payment Received!';
        }
    } else {
        echo 'Confirmation not found, please recontact the booking agency';
        header("location:../404.php");
    }
}

/**
 * Updates the reservation status to paid and adds the paypal confirmation
 */
function payPal()
{
    $conn = connection();
    $paypalconfirmation = $_GET['id'];
    $dbconfirm = $_SESSION['confirm'];
    $money = 'paid';
    $stmt = $conn->prepare("UPDATE reservation SET status=?, paypalID=? WHERE confirmation=?");
    $stmt->bind_param('sss', $money, $paypalconfirmation, $dbconfirm);
    $stmt->execute();
    echo '<h2>The Amount is paid, Thank you for booking with us :)</h2>';
    header('Refresh: 3; url=http://localhost/myProject/');
    if ($_SESSION['type'] == 'user') {
        header('Refresh: 3; url=http://localhost/myProject/user/dashboard.php');
    }
}

/**
 *Not avaiable dates printed from database
 */
function datesNotAvailable()
{
    $conn = connection();
    $unavailable = array();
    $sql = "SELECT * FROM reservation WHERE `dato` > NOW()";
    if ($result = $conn->query($sql)) {
        while ($row = $result->fetch_assoc()) {
            $unavailable[] = $row['dato'];
        }
    }
    echo implode(",", $unavailable);
}

/**
 * @param $to_email
 * @param $invoice
 * @param $price
 * sends email to customer with invoice id and amount owed.
 */
function sendEmail($to_email, $invoice, $price)
{
    $subject = "Customer Invoice : $invoice";
    $body = "
 Dear Customer, This is to inform you that you have outstanding balance of $price GBP that needs to be paid for the booking to be registered in our system
 http://localhost/myProject/shared/reservation.php?confirmation=$invoice\r\n

  Kind Regards,
  The booking Agency";
    $headers = 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers = "From: Yasin Zamani K";
    if (mail($to_email, $subject, $body, $headers)) {
        echo "Email successfully sent to $to_email";
    } else {
        echo "Email sending failed...";
    }

}

/**
 * @param $name
 * @param $email
 * @param $subject
 * @param $message
 * @param $dato
 * @param $guests
 * Contact Us form, inserts the data to table.
 */
function addContactUs($name, $email, $subject, $message, $dato, $guests)
{
    $conn = connection();
    $sql = "INSERT INTO contact(name,email,subject,message,dato,guests) VALUES(?,?,?,?,?,?);";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssssi", $name, $email, $subject, $message, $dato, $guests);
    $stmt->execute();
    $stmt->close();
}

/**
 * @param $name
 * @param $confirmation
 * @param $meal
 * @param $price
 * my try on generating a pdf file
 */
function createPdf($name, $confirmation, $meal, $price)
{
    $today = date("Y-m-d");
    $pdf = new FPDF('P', 'mm', 'A4');

    $pdf->AddPage();

    $pdf->SetFont('Arial', 'B', 20);

    $pdf->Cell(45, 10, '', 0, 0);
    $pdf->Cell(5, 5, "Your Invoice for booking", 0, 0);
    $pdf->Cell(59, 10, '', 0, 1);

    $pdf->Cell(59, 5, 'Details', 0, 1);

    $pdf->SetFont('Arial', '', 10);

    $pdf->Cell(25, 5, 'Name :', 0, 0);
    $pdf->Cell(34, 5, " $name ", 0, 1);
    $pdf->Cell(25, 5, 'Invoice Date:', 0, 0);
    $pdf->Cell(34, 5, $today, 0, 1);
    $pdf->Cell(25, 5, 'Invoice No:', 0, 0);
    $pdf->Cell(34, 5, $confirmation, 0, 1);

    $pdf->Output();

}

/**
 * @param $to_email
 * @param $txt
 * Replys to the mails recived in admin dashboard
 */
function reply($to_email, $txt)
{
    $subject = "Contact Us form reply";
    $body = $txt;
    $headers = 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers = "From: Yasin Zamani K";
    if (mail($to_email, $subject, $body, $headers)) {
        echo "Email successfully sent to $to_email";
    } else {
        echo "Email sending failed...";
    }

}

/**
 * @param $email
 * @param $name
 * Updates Reservation when details of account changes
 */
function updateReservation($email, $name)
{
    $conn = connection();
    $emails = $_SESSION['email'];
    $stmt = $conn->prepare("UPDATE reservation SET email=?, firstname=? WHERE email=?");
    $stmt->bind_param('sss', $email, $name, $emails);
    $stmt->execute();

}

/**
 * @param $name
 * @param $email
 * @param $password
 * Changes details of user profile
 */
function UpdateDetails($name, $email, $password)
{
    $conn = connection();
    $emails = $_SESSION['email'];
    updateReservation($emails, $name);
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
    $stmt = $conn->prepare("UPDATE users SET username=?, email=?,password=? WHERE email=?");
    $stmt->bind_param('ssss', $name, $email, $hashedPassword, $emails);
    $stmt->execute();

}


?>
