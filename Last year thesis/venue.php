<!--
 Author : Yasin Zamani Konari
 email: yzk2@protonmail.com
 -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CLASSIC VENUE</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="shared/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
    <div class="header">
        <nav class="navbar navbar-expand-sm">
            <div class="container justify-content-end">
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse"
                        data-bs-target="#CollapseContent">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="CollapseContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0 nav-pills">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="aboutus.php">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="venue.php">Venue</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contactus.php">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signup.php">Register</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signin.php">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container">
    <h3 class="text-muted">CLASSIC VENUE</h3>
    <div id="slideshow" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/1920x503.jpg" class=" img-fluid d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <p class="bg-dark">We have the experience and specialities of fulfilling your dreams..</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="img/1920x502.jpg" class=" img-fluid d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <p class="bg-dark">We look after our customers, and we are easy to work with </p>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#slideshow" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#slideshow" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <div class="row information">
        <div><p class="text-center display-5 ">What we offer with our bundles</p>
            <br>
        </div>
        <div class="col row-cols-7 row-cols-md-7 text-center">
            <h4 class="text-center">Bundles</h4>
            <p>Our venue offers three different types of Bundles for your event needs. </p>

            <h4 class="text-center">Information about catering</h4>
            <p>We have multiple contacts with caterers around us, they are experienced for your wanting and needs. </p>

            <h4 class="text-center">Photographer</h4>
            <p>We can provide photography of the event and videography, these are professional with multiple of years of
                experience</p>

            <div class="row">
                <h4 class="pb-1 text-center">Where to Find our venue and opening hours</h4>
                <div class="col">
                    <p>Our venue can be Found here :</p>
                    <div class="gmap_canvas">
                        <iframe width="538px" height="350px" frameborder="0" style="border: 0;" allowfullscreen=""
                                aria-hidden="false" tabindex="0" id="gmap_canvas"
                                src="https://maps.google.com/maps?q=London&t=&z=15&ie=UTF8&iwloc=&output=embed"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        <style>.gmap_canvas {
                                overflow: hidden;
                                background: none !important;
                            }

                            width:

                            538
                            px</style>
                    </div>
                </div>
                <div class="col pb-1">
                    <div class="col">
                        <p class="text-center pt-5">Our Office Opening Hours : </p>
                        <p class="text-center">Sunday Closed</p>
                        <p class="text-center">Monday 9:00-22:00</p>
                        <p class="text-center">Tuesday 9:00-22:00</p>
                        <p class="text-center">Wednesday 9:00-22:00</p>
                        <p class="text-center">Thursday 9:00-22:00</p>
                        <p class="text-center">Friday 9:00-23:30</p>
                        <p class="text-center">Saturday 14:00-23:30</p>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<div class="container text-center">
    <h1 class="display-3">Pricing</h1>
    <p class="lead">Our Venue offer the following Bundles</p>
</div>
<div class="container text-center">
    <section id="prices">
    </section>
    <div class="row row-cols-1 row-cols-md-3">
        <div class="col">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h2>No Bundle</h2>
                    <h5>2899 GBP</h5>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled mt-4 mb-4">
                        <li>Per seating 5 GBP</li>
                        <li>No Food catering</li>
                        <li>No Photographer</li>
                        <li>.</li>
                    </ul>
                    <a class="btn btn-sm btn-success" href="signup.php">Book Now</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card shadow-lg">
                <div class="card-header">
                    <h2>Bundle 1</h2>
                    <h5>3599 GBP</h5>
                </div>
                <div class="card-body shadow-lg">
                    <ul class="list-unstyled mt-4 mb-4">
                        <li>Per seating 5 GBP</li>
                        <li>Food Catering</li>
                        <li>DJ</li>
                        <li>Photographer Included</li>
                    </ul>
                    <a class="btn btn-sm btn-success" href="signup.php">Book Now</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h2>Bundle 2</h2>
                    <h5>3099 GBP</h5>
                </div>
                <div class="card-body shadow-sm">
                    <ul class="list-unstyled mt-4 mb-4">
                        <li>Per seating 5 GBP</li>
                        <li>Photographer Included</li>
                        <li>No Food catering</li>
                        <li>.</li>
                    </ul>
                    <a class="btn btn-sm btn-success" href="signup.php">Book Now</a>
                </div>
            </div>
        </div>
        <div class="col pt-5">
            <h2>Custom Offer ?</h2>
            <ul class="list-unstyled mt-4 mb-4">
                <li>Per seating 5 GBP</li>
            </ul>
            <a class="btn btn-sm btn-success" href="contactus.php">Contact Us</a>
        </div>
    </div>
</div>
<div class="container">
    <footer class="footer">
        <p>© CLASSIC VENUE 2021</p>
    </footer>

</div>

</body>
</html>
