<?php
/*
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
session_start();
include 'functions.php';

if (isset($_POST['submit'])) {

    $email = $_POST["user_email"];
    $pwd = $_POST["user_password"];

    loginAccount($email, $pwd);
}
