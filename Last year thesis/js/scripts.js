/*
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */

/*
Contact us details from inputs sent to POST via ajax request.
 */
$(function(){
    $("#details").on("submit", function(e) {
        e.preventDefault();
        var submission = {
            'name' : $('input[name=name]').val(),
            'email' : $('input[name=email]').val(),
            'subject' : $('input[name=subject]').val(),
            'message' : $('textarea[name=message]').val(),
            'guests'  : $('input[name=guests]').val(),
            'date'    : $('input[name=dataoKontroll]').val(),
        };
        $.ajax({
            url: "contact.php",
            type: "post",
            data: submission,
            success: function() {
               if($('#myModal').modal('toggle')){
                  setInterval('window.location.reload()', 4000);

               }

            }
        });
    });
})
/*
gets the database dates and blocks of the dates that are not supposed to be there.
 */
$(document).ready(function(){
    var date = new Date();
    date.setDate(date.getDate()-1);
    $.get("avadates.php", function(dates){
        disabledDates = dates.split(',');
        $('#dataoKontroll').datepicker({
            startDate: date,
            format: 'yyyy-mm-dd',
            datesDisabled: disabledDates,
            orientation: "bottom auto",
            autoclose: true,
        });
    });
});