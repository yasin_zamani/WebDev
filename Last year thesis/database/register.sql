-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2021 at 09:07 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `register`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `dato` varchar(255) NOT NULL,
  `guests` int(255) NOT NULL,
  `status` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `message`, `dato`, `guests`, `status`) VALUES
(1, 'Yasin Zamani Konari', 'yzk@aber.ac.uk', 'abca', 'ababab', '2021-04-13', 25, 1),
(5, 'Yasin Zamani Konari', 'yzk@aber.ac.uk', 'abab', 'asldkjaskdjaskljdklasjkldjaskljdklasjkdjaklsdjlkajskldjaklsjdklajskdljsakldjlkasjdkasjdklajskdlasjcklasklncnasclkasncklnasklcasklnckalsncklasncklnasklcnasklncklasncklnasklcnasknckalscnklasncklnasklcnasklcnaklsncklsancklansklcnasklcnaklscnklasncklanscklnas', '2021-04-13', 25, 1),
(6, 'Yasin Zamani Konari', 'yzk@aber.ac.uk', 'abab', 'anothertest', '2021-04-13', 25, 1),
(22, 'Yasin Zamani Konari', 'yzk2@protonmail.com', 'Party', 'No bundle, we will provide our own catering, photography and dj.', '2021-04-28', 32, 1),
(23, 'Yasin Zamani Konari', 'yzk2@protonmail.com', 'Party Phone', 'The request of the message.<', '2021-04-28', 32, 1),
(24, 'My Name is..', 'yzk@aber.ac.uk', 'Form Submission', 'Form text123', '2021-04-28', 12, 1),
(25, 'Yasin Zamani Konari', 'yzk@aber.ac.uk', 'Party', '123', '2021-05-03', 23, 1),
(26, 'Yasin Zamani Konari', 'yzk@aber.ac.uk', 'Party', 'Reply demo ', '2021-05-03', 23, 1),
(27, 'Yasin Zamani Konari', 'yzk@aber.ac.uk', 'Party', 'Test', '2021-04-29', 12, 0),
(28, 'Yasin Zamani Konari', 'yzk@aber.ac.uk', 'Party Phone', 'ababab', '2021-04-29', 23, 1),
(29, 'Yasin Zamani', 'yzk@aber.ac.uk', 'Hello World', 'Hello World', '2021-05-11', 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `reservation_id` int(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `dato` varchar(255) NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT 'not paid',
  `confirmation` varchar(255) NOT NULL,
  `guests` int(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `package` varchar(255) DEFAULT NULL,
  `paypalID` varchar(255) DEFAULT NULL,
  `total` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`reservation_id`, `firstname`, `email`, `contact`, `dato`, `status`, `confirmation`, `guests`, `message`, `package`, `paypalID`, `total`) VALUES
(133, 'useraccount', 'user@gmail.com', '07999999888', '2021-04-19', 'not paid', '7bgiug3g', 23, 'NO DJ', 'nobundle', NULL, 3014),
(164, 'Edel Sherratt', 'useraccount@gmail.com', '07594965318', '2021-04-30', 'paid', 't4wdaygc', 13, 'Only vegetarian catering, photographer included and no dj.', 'meal1', '0DF029391E0233625', 3664),
(165, 'I have changed name', 'mskonari@broadpark.no', '07594965318', '2021-05-10', 'paid', '6aii6va0', 32, 'text changed and date 123123 123', 'meal1', '73E57188LA300554M', 3759),
(168, 'Testt Name', 'testaccount1@gmail.com', '07999999888', '2021-05-04', 'paid', 'xx2fqsq8', 12, 'adadada', 'meal1', '62N27267889978438', 3659),
(170, 'My Name is..', 'yzk@aber.ac.uk', '07999999888', '2021-04-29', 'not paid', 'uxrrpcs4', 0, '232323', 'nobundle', NULL, 3999),
(171, 'My Name is..', 'yzk@aber.ac.uk', '07999999888', '2021-04-29', 'not paid', 'hj8oczic', 0, '123', 'nobundle', NULL, 1337),
(172, 'Yasin Zamani Konari', 'yzk3aber.ac.uk', '07594965318', '2021-05-05', 'paid', '2cprsod2', 50, 'My Message about booking', 'meal2', '56D82766YX657294J', 3349),
(173, 'Yasin Zamani Konari', 'yzk2@protonmail.com', '07594965318', '2021-05-03', 'not paid', 'xm8ksddk', 0, 'a', 'nobundle', NULL, 2424),
(174, 'Yasin Zamani Konari', 'yasink921222@gmail.com', '07594965318', '2021-05-06', 'not paid', '0qsyu0un', 4, '123123', 'meal2', NULL, 3119),
(175, 'Yasin Zamani Konari', 'yzk@aber.ac.uk', '07594965318', '2021-04-29', 'not paid', 'j2qa7ib7', 0, 'test23', 'nobundle', NULL, 33333),
(176, 'Yasin Zamani Konari', 'yzk3aber.ac.uk', '07123123122', '2021-05-06', 'paid', 'ca0iewha', 12, 'Important detail about my booking edited', 'nobundle', '4MP48130ES8736922', 2959),
(177, 'Yasin Zamani Konari', 'yzk33@aber.ac.uk', '07123123122', '2021-05-11', 'paid', 'qmpjdygp', 23, 'abcdef', 'meal2', '5CW730596B3017211', 3214);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(6) UNSIGNED NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `user_type`) VALUES
(5, 'Yasin Zamani', 'yzk@aber.ac.uk', '$2y$10$S/3vYRjernjqp5/nXOYbs.qbxq/QIv/ljlAaqMWUaPWgwgjOafj5i', 'admin'),
(9, 'Assistant 2', 'att@gmail.com', '$2y$10$szmm6kPIDucuC4WC824/TubnbNvAyXiMEN1vqUoC6EIjINsNe1gQy', 'att'),
(32, 'Edel Sherratt', 'useraccount@gmail.com', '$2y$10$KwYiDH7mKkpv7xAXpLIdSumGTycRnDiyYdLpwkuD05aHz9iPviEWK', 'user'),
(45, 'Yasin Zamani Konari', 'mskonari@broadpark.no', '$2y$10$HdJO1L7aW6c9fxK8RW5VheVkqOZJIZjWmkQQxVNPGwKiubBryxXNq', 'user'),
(47, 'useraccount', 'user@gmail.com', '$2y$10$LwWQHDPRXRQUY8l/12RhoOKBQeqfN4mqRZCbmL62DO7rHiYIZDJW6', 'user'),
(50, 'Yasin Zamani Konari', 'yasink921222@gmail.com', '$2y$10$D1/Ke.hrGhLF1BH7se0xpuf92VPeH1nF8WQNTmY.vDLT1L4MCkeNm', 'user'),
(51, 'Edel Sherratt', 'eds@aber.ac.uk', '$2y$10$4V3fNwYLJ0hr4cy/koUUneoIf1Vo6bDrDdBbOsfobOxXAtCONOz5G', 'att'),
(53, 'Yasin Z K', 'yzk3@aber.ac.uk', '$2y$10$fVrHAiXjgy0Vka54Oi4xHOaPFyGnR4adEHMfRb1fWyFYCYP34kYDm', 'user'),
(54, 'Demo Account', 'demo@account.com', '$2y$10$MZCJ8RuwNYWjxhSRc22YmONhjMRDuWaegsOv7J0BAu1hcVyUhBKue', 'att');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`reservation_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `reservation_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
