<!--
 Author : Yasin Zamani Konari
 email: yzk2@protonmail.com
 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Home Page">
    <meta name="author" content="Yasin Zamani Konari">

    <title>CLASSIC VENUE</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="shared/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
    <div class="header">
        <nav class="navbar navbar-expand-sm">
            <div class="container justify-content-end">
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse"
                        data-bs-target="#CollapseContent">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="CollapseContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0 nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="aboutus.php">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="venue.php">Venue</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contactus.php">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signup.php">Register</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signin.php">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <br>
    <h3 class="text-muted">CLASSIC VENUE</h3>
    <br>


    <h1 class="display-5">Why our venue?</h1>
    <div id="slideshow" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/1920x500.jpg" class=" img-fluid d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <p class="bg-dark">It is our attention to the small details, scheduling timelines and keeping a
                        close eye to your budget and the calendar. This makes us stand out from the rest!</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="img/1920x501.jpg" class=" img-fluid d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <p class="bg-dark">Our Prices are fair for you to enjoy the stay with us, there are no surprise
                        bills. Any unexpected or additional expenses must be approved by you for us to take further
                        actions.</p>
                    <a class="btn btn-sm btn-success" href="venue.php#prices">View our Prices</a>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#slideshow" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#slideshow" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <div class="row information">
        <div><p class="text-center display-5 ">What we offer</p>
            <br>
        </div>
        <div class="col-lg-6">
            <h4>Weddings</h4>
            <p>We have special team and experts for wedding management, our team will offer their best and the best
                experience in our venue. Weddings require us to plan and timetable, please book one month in
                advance.</p>

            <h4 class="pt-5">Parties</h4>
            <p>We are open to any type of parties or customer offer, for this to happen please keep in mind that a
                custom request is required either by contacting us by the form or calling our office.</p>
            <h4 class="pt-5">Birthday Parties</h4>
            <p>We would be glad to let you use our venue for the birthday celebration. This type of booking requires us
                to plan and manage the venue for choice. Please kindly book the venue by yourself or use the contact us
                form if needed assistant.</p>
        </div>

        <div class="col-lg-6">
            <h4>Catering</h4>
            <p>We have connection with multiple carters, choosing bundle 3 please in comment section type what type of
                food you prefer to be served. Each seating will serve plates paid. </p>
            <p>Our carters serve these foods: <b>Vegetarian, English, Mediterranean and Indian</b></p>
            <h4 class="pt-5">Photography</h4>
            <p>We have multiple contact with people within the photography field, these people are experts in
                photography and videography. They are very reliable and willing to do custom photography too! </p>

            <h4 class="pt-2">DJ</h4>
            <p>Our venue is equipped with DJ, soundboards and lighting effects. The DJ Software can load up music from
                your favourite playlist and can be played in the venue.</p>
        </div>
    </div>
    <hr>

    <footer class="footer">
        <p>© CLASSIC VENUE 2021</p>
    </footer>

</div>
</body>
</html>
