<?php
/*
 * Destroys every session on this site.
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
session_start();
unset($_SESSION["type"]);
session_destroy();
header("Location:../index.php");

