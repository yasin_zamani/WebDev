<?php
/*
 * Deleting reservation
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
include "../connection.php";
session_start();
switch (isset($_SESSION['type'])) {
    case 'admin' or 'att':
        $conn = connection();
        $confirmation = $_GET['delete'];
        $sql = "DELETE from reservation WHERE confirmation = ?;";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $confirmation);
        $stmt->execute();
        break;
    default:
        echo 'invalid session';
        break;
}
