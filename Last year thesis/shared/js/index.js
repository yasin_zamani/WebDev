/*
 *
 * PayPal sample code which is used in the system.
 */

paypal.Buttons({
    style: {
        color: 'blue',
        shape: 'pill'
    },
    createOrder: function (data, actions) {

        return actions.order.create({
            purchase_units: [{
                amount: {
                    value: $("#amount").text(),

                }
            }]
        });
    },
    onApprove: function (data, actions) {
        return actions.order.capture().then(function (details) {
            console.log(details)
            //alert(' ' + details.payer.email_address); //pass
            //alert(' ' + details.id); //pass
            window.location = "../shared/success.php?id=" + details.id;

        })
    },
    onCancel: function (data) {
        alert("Payment cancelled");
    }
}).render('#paypal-payment-button');

$(document).bind("contextmenu", function (e) {
    e.preventDefault();
});
