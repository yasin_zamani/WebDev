<?php
include "../../connection.php";
$paid = 'paid';
$conn = connection();
$dates = array();
$sql = "SELECT * FROM reservation WHERE `dato` > NOW() and status =?;";
$stmt = $conn->prepare($sql);
$stmt->bind_param('s',$paid);
$stmt->execute();
$result = $stmt->get_result();
while ($row = $result->fetch_assoc()) {
    $dates[] = $row['dato'];
}
echo implode(",", $dates);