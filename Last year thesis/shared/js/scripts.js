/*
Author : Yasin Zamani Konari
Email : yzk2@protonmail.com
*/

/*
When the button Delete is pressed, it will execute AJAX script for deleting the row from the table without refreshing.
*/
var row;
var date = new Date();
$(function () {
    $(".delbutton").click(function (e) {
        e.preventDefault();
        row = $(this).closest('tr');
        console.log(row);
        if (confirm("Are you sure you want to delete this Query? There is NO undo!")) {
            // send in our php script, to delete row.
            $.ajax({
                type: "GET",
                url: "../shared/delete.php",
                data: 'delete=' + $(this).attr("delete"),
                success: function () {
                    row.addClass("bg-danger");
                    row.hide(3000, function () {
                        row.hide();
                        //  location.reload();

                    })
                }
            });

        }
        return false;
    });

});

/*
Simple search function to find matching confirmation on the table we have.
*/

$(function () {
    $("#search-input").on("keyup", function () {
        var search = $(this).val();
        $("table tr").each(function (index) {
            if (index !== 0) {
                $rowOfTable = $(this);
                var searchID = $rowOfTable.find("td").text();
                if (searchID.indexOf(search) !== 0) {
                    $rowOfTable.hide();
                } else {
                    $rowOfTable.show();
                }
            }
        });
    });
});
/*
gets the available dates from the database
 */
$(document).ready(function () {
    date.setDate(date.getDate()+1);
    $.get("/myProject/shared/avadates.php", function (dates) {
        disabledDates = dates.split(',');
        $('#dataoKontroll').datepicker({
            startDate: date,
            format: 'yyyy-mm-dd',
            datesDisabled: disabledDates,
            orientation: "bottom auto",
            autoclose: true,
            daysOfWeekDisabled: "0,6",
        });
    });
});
/*
Changes the details of booking and updates the database via POST request
 */
var row;
$(document).ready(function () {
    $(document).on('click', 'a[update=update]', function () {
        //alert($(this).data('id'));
        var fetchid = $(this).data('id');
        row = $(this).closest('tr');
        console.log(fetchid);
        var name = $('#' + fetchid).children('td[row=name]').text();
        var email = $('#' + fetchid).children('td[row=email]').text();
        var contact = $('#' + fetchid).children('td[row=contact]').text();
        var guests = $('#' + fetchid).children('td[row=guests]').text();
        var dato = $('#' + fetchid).children('td[row=dato]').text();
        var message = $('#' + fetchid).children('td[row=message]').text();
        var pkage = $('#' + fetchid).children('td[row=package]').text();
        var id = $('#' + fetchid).children('td[row=key]').text();
        // console.log(row);
        //  console.log(name);
        //  console.log(email);
        //  console.log(contact);
        //  console.log(guests);
        //  console.log(dato);
        //  console.log(message);
        //  console.log(id);
        //  console.log(pkage);
        $('#record').val(row);
        $('#dataoKontroll').val(dato);
        $('#secretkey').val(id);
        $('#name').val(name);
        $('#email').val(email);
        $('#message').val(message);
        $('#guests').val(guests);
        $('#phone').val(contact);
        $('#package-selection').val(pkage);
        $('#myModal').modal('toggle');

    })

})
$(document).ready(function () {
    $("#tot_amount_custom").hide();
    $(document).on('click', '.accept', function () {
        var name = $('#name').val();
        var email = $('#email').val();
        var id = $('#secretkey').val();
        var guests = $('#guests').val();
        var message = $('#message').val();
        var dato = $('#dataoKontroll').val();
        var package = $('#package-selection').val();

        if (confirm("Are you sure you want to change this Query? There is NO undo!")) {
            // send in our php script, to update rows.
            $.ajax({
                type: "POST",
                url: "../shared/edit.php",
                data: {
                    name: name,
                    email: email,
                    id: id,
                    guests: guests,
                    message: message,
                    dato: dato,
                    package: package
                },
                success: function () {
                    $('#myModal').modal('toggle');
                    row.addClass("bg-success");
                    row.hide(3000, function () {
                        row.addClass("");
                        row.show();
                        location.reload();

                    })
                }
            });

        }
    })
});
/*
gets customer information about booking in a modal.
 */
$(document).on('click', 'a[info=details]', function () {
    var fetchid = $(this).data('id');
    //alert(fetchid);
    $('#myModal').modal('toggle');
    $.ajax({
        type: "POST",
        url: "../shared/customerinfo.php",
        data: {confirm: fetchid},
        success: function (info) {
            $("#information").html(info);
        }
    });

});
var fetchid;
$(document).on('click', 'a[reply=reply]', function () {
    fetchid = $(this).data('reply');
    console.log(fetchid);
    //console.log(fetchid);
    $('#myModal').modal('toggle');
    $("#submitmsg").on("click", function () {
        var submission = {
            'message': $('textarea[name=message]').val(),
            'id': fetchid,
        };
        $.ajax({
            url: "reply.php",
            type: "post",
            data: submission,
            success: function () {
                $('#myModal').modal('toggle');
                alert("Message sent");
                location.reload();
            }
        });
    });
});

$(document).on('click', 'a[click=yes]', function () {
    //console.log(fetchid);
    $('#myModal').modal('toggle');
    $("#submitmsg").on("click", function () {
        var submission = {
            'message': $('textarea[name=message]').val(),
            'id': fetchid,
        };
        $.ajax({
            url: "reply.php",
            type: "post",
            data: submission,
            success: function () {
                $('#myModal').modal('toggle');
                alert("Message sent");
                location.reload();
            }
        });
    });
});

/*
Deletes Assistant from the database
 */
$(function () {
    $(".delebutton").click(function (e) {
        e.preventDefault();
        row = $(this).closest('tr');
        var assistantID = $(this).attr("delete");

        if (confirm("Are you sure you want to delete this Query? There is NO undo!")) {
            // send in our php script, to delete row.
            $.ajax({
                type: "GET",
                url: "delassistant.php",
                data: 'delete=' + assistantID,
                success: function () {
                    row.addClass("bg-danger");
                    row.hide(3000, function () {
                        row.hide();
                        //  location.reload();

                    })
                }
            });

        }
        return false;
    });

});

/*
Changes details of user
 */
$(function(){
    $("#confirmchanges").click(function () {
        console.log('User Changing Account Details Enabled..');
        if($("#confirmchanges").on('click')) {
            $('#name').prop('readonly', false);
            $('#email').prop('readonly', false);
            $('#password').prop('readonly', false);
            $(':input[type="submit"]').prop('disabled', false);
        }
    });
    })
/*
Deleting user account from database
 */
$(function() {
    $("#delete").click(function () {
        var account = $(this).attr('deleteaccount');
        if (confirm("Are you Sure You want to Delete Your account? There is no undo!")) {
            $.ajax({
                type: "GET",
                url: "deleteacc.php",
                data: 'delete=' + account,
                success: function () {
                    location.replace("leave.php");
                }
            });

        }
        return false;

    });
})
/*
Customer offer tick box Admin and assistant.
 */
$(document).on('click', '#custom', function () {
    if($("#custom").is(':checked')){
        $("#tot_amount_custom").show();
        $('#guests').prop('readonly', true);
        $('#guests').val(0);
        $('#package-selection').val("nobundle");
    } else {
        $('#guests').prop('readonly', false);
        $("#tot_amount_custom").hide();
        $('#tot_amount_custom').val('');
        $('#package-selection').val("");
    }
});

