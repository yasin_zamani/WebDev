<?php
/*
 * display customer information when they go trough the payment gateway
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
include "../connection.php";
$confirm = $_POST['confirm'];
$conn = connection();
$stmt = $conn->prepare("SELECT * FROM reservation where reservation_id=?");
$stmt->bind_param('i',$confirm);
$stmt->execute();
$result = $stmt->get_result();
$row = $result->fetch_assoc();
echo 'Customer Name : ', $row['firstname'];
echo "<br>";
echo 'Customer Amount Paid : ', $row['status'];
echo "<br>";
echo 'Customer Confirmation : ', $row['confirmation'];
echo "<br>";
echo 'Important Messages about booking : ', $row['message'];