<!DOCTYPE html>
<!--
 Author : Yasin Zamani Konari
 email: yzk2@protonmail.com
 -->
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CLASSIC VENUE</title>

    <link href="..\css\bootstrap.css" rel="stylesheet">
    <link href="..\css\style.css" rel="stylesheet">
    <script src="..\js\jquery.min.js"></script>
    <script src="https://www.paypal.com/sdk/js?client-id=AWT4VtJmKNRp3oxBKoJbTZumt1Pzq8eRdnypfxvEauk_tf0ut0491WYtjBcxU_7IwpyaIaIsX9zOSoDN&currency=GBP"></script>
    <script src="js/index.js"></script>
</head>


<body>
<div class="d-flex" id="wrapper">
    <div class="container">
    </div>
</div>
</div>

</body>
</html>
<?php
session_start();
include '../functions.php';
if (isset($_POST['submit'])) {
    $price = 2899;
    if(($_POST['tot_amount_custom']) > 0) {
        $price = $_POST['tot_amount_custom'];
    }
    $name = $_POST['visitor_name'];
    $email = $_POST['visitor_email'];
    $phone = $_POST['visitor_phone'];
    $guests = $_POST['total_guests'];
    $guestincrement = $guests * 5;
    $date = $_POST['date'];
    $package = $_POST['package_preference'];
    $message = $_POST['visitor_message'];
    $confirmation = $_GET['confirmation'];
    if ($package == 'meal2') {
        $price += 200 + $guestincrement;
    } else if ($package == 'meal1') {
        $price += 700 + $guestincrement;
    } else if ($package == 'nobundle') {
        $price += $guestincrement;
    }
    addBooking($name, $email, $phone, $date, $confirmation, $guests, $message, $package, $price);
} else {
    $confirmation = $_GET['confirmation'];
    customerInfo($confirmation);
    $_SESSION['confirm'] = $confirmation;
}

?>
