<?php
/*
 * Edit reservation
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
include "../connection.php";
session_start();
if (($_SESSION['type']) == 'admin' or 'att') {
    $conn = connection();
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST["message"];
    $date = $_POST["dato"];
    $package = $_POST["package"];
    $guests = $_POST["guests"];
    $id = $_POST['id'];
    $stmt = $conn->prepare("UPDATE reservation SET firstname=?, email=?, message=?, dato=?, package=?, guests=? WHERE confirmation=?");
    $stmt->bind_param("sssssis", $name, $email, $message, $date, $package, $guests, $id);
    $stmt->execute();
} else {
    echo 'invalid session';
}
