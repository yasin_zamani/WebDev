<!--
 Author : Yasin Zamani Konari
 email: yzk2@protonmail.com
 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Contact us Page">
    <meta name="author" content="Yasin Zamani Konari">

    <title>CLASSIC VENUE</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="shared/js/scripts.js"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
          integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
          crossorigin="anonymous"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
            integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
            crossorigin="anonymous"></script>
</head>

<body>
<div class="container">
    <div class="header">
        <nav class="navbar navbar-expand-sm">
            <div class="container justify-content-end">
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse"
                        data-bs-target="#CollapseContent">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="CollapseContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0 nav-pills">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="aboutus.php">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="venue.php">Venue</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="contactus.php">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signup.php">Register</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signin.php">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="container">
        <!-- Contact Us Section -->
        <h3 class="text-muted">CLASSIC VENUE</h3>
        <div class="container-fluid shadow-lg ">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="text-center">CONTACT US</h2>
                    <div class="col-sm-3">
                        <h5>ADDRESS :</h5>
                        <p>A53 Guild Street,</p>
                        <p>LONDON</p>
                        <p>EC4A 2NY</p>
                        <a href="tel:+447812345678">+447812345678</a>
                        <p></p>
                        <p><a href="mailto:yzk2@protonmail.com">yzk2@protonmail.com</a></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-7">
                        <form id="details" name="details" action="" method="post">
                            <div class="mb-3">
                                <div class="mb-3">
                                    <input type="text" name="name" class="form-control" id="name" pattern="([A-zÀ-ž\s])+" minlength="5" maxlength="20"
                                           required
                                           placeholder="Your Name"/>
                                </div>
                                <div class="mb-3">
                                    <input type="email" class="form-control" name="email" id="email" required
                                           placeholder="Your Email"/>
                                </div>
                                <div class="md-3 mb-3">
                                    <input type="number" id="guests" name="guests" min="1" max="50" placeholder="Guests"
                                           required class="form-control">
                                </div>
                                <div class="md-3 mb-3">
                                    <input id="dataoKontroll" name="dataoKontroll" placeholder="Date" required
                                           class="form-control">
                                </div>
                            </div>
                            <div class="mb-3">
                                <input type="text" class="form-control" name="subject" id="subject" required
                                       placeholder="Subject"/>
                            </div>
                            <div class="mb-3">
                                <textarea class="form-control" name="message" id="message" required
                                          placeholder="Send us a message about your budget etc ..."
                                          style="height: 160px;"></textarea>
                            </div>
                            <button name="input" id="tgl1" class="btn btn-block btn-primary">Contact Us</button>
                        </form>
                    </div>
                    <div class="col-lg-5">
                        <div class="gmap_canvas">
                            <iframe width="538px" height="350px" frameborder="0" style="border: 0;" allowfullscreen=""
                                    aria-hidden="false" tabindex="0" id="gmap_canvas"
                                    src="https://maps.google.com/maps?q=London&t=&z=15&ie=UTF8&iwloc=&output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            <style>.gmap_canvas {
                                    overflow: hidden;
                                    background: none !important;
                                }

                                width:

                                538
                                px</style>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <p>© CLASSIC VENUE 2021</p>
                </footer>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Modal from w3s -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <button type="button" class="close d-flex justify-content-end" data-dismiss="modal">&times;</button>
                    <div class="modal-header">
                        <h4 class="modal-title">Thank you for sending your inquiry, We will be contacting you soon
                            :)</h4>
                    </div>
                    <div class="d-flex justify-content-center">
                        <img src="img/success.png" width="150" height="150" alt="sent">
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
