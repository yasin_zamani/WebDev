<?php
/*
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
include 'functions.php';
if (isset($_POST['submit'])) {
    $user = $_POST['user_name'];
    $email = $_POST['user_email'];
    $pwd = $_POST['user_password'];
    $pwd2 = $_POST['user_password2'];
    $user_type = 'user';
    if ($_POST['type']) {
        $user_type = 'att';
        header("location:admin/addassistant.php?success=yes");
    }
    if (pwdMatch($pwd, $pwd2) !== false) {
        if ($user_type == 'user') {
            header("location:signup.php?error=pwdmismatch");
        } else {
            header("location:admin/addassistant.php?error=pwdmismatch");

        }
        exit();
    }

    if (emailExists($email) !== false) {
        if ($user_type == 'user') {
            header("location:signup.php?error=emailExists");
        } else {
            header("location:admin/addassistant.php?error=emailExists");
        }
        exit();
    } else {
        header("location:admin/addassistant.php?success=yes");

    }
    CreateAccount($user, $email, $pwd, $user_type);

}

?>
