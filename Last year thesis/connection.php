<?php
/*
 * Connection to MariaDB server
 * Author : Yasin Zamani Konari
 * Email : yzk2@protonmail.com
 */
/**
 * @return mysqli connection
 */
function connection()
{
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    return new mysqli('localhost', 'root', '', 'register');
}


