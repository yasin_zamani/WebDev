<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Yasin Zamani Konari">

    <title>CLASSIC VENUE</title>

    <link href="..\css\bootstrap.css" rel="stylesheet">
    <link href="..\css\style.css" rel="stylesheet">
    <script src="..\js\jquery.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
          integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
          crossorigin="anonymous"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
            integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
            crossorigin="anonymous"></script>
    <script src="../shared/js/scripts.js"></script>
</head>
<body>
<?php
session_start();
if ($_SESSION['type'] == 'user') {

} else {
    echo 'session not set :(!)';
    header("location:../signin.php?error=invalidSession");
}
?>


<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="list-group">


        <br>
        <a href="dashboard.php" class="list-group-item list-group-item-action active">Book Venue</a>
        <a href="venueava.php" class="list-group-item list-group-item-action">Venue Availability</a>
        <a href="viewreservations.php" class="list-group-item list-group-item-action">Manage Reservation</a>
        <a href="upcomingevents.php" class="list-group-item list-group-item-action">Upcoming Events</a>
        <a href="myaccount.php" class="list-group-item list-group-item-action">My Details</a>
        <a href="#" id="delete" name="delete" deleteaccount=<?php echo $_SESSION['email']; ?> class="list-group-item
           list-group-item-action">Delete Account</a>
        <a href="../shared/logout.php" class="list-group-item list-group-item-action">Log out</a>
    </div>

    <!-- Page Content -->
    <?php
    include '..\functions.php';
    $email = $_SESSION['email'];
    $conn = connection();
    $confirmation = confirmationNumber();
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $query = "SELECT * FROM users WHERE email =?;";
    $stmt = $conn->prepare($query);
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();
    ?>
    <div class="container">
        <h1 class="mt-1 text-center">Booking Venue</h1>
        <form class="row g-3" action="../shared/reservation.php?confirmation=<?php echo $confirmation; ?>" method="post"
        =name"myform" id="myform">
        <div class="col-md-4">
            <label for="name">Customer Name</label>
            <input class="form-control" type="text" id="name" name="visitor_name" placeholder="John Doe"
                   value="<?php echo $row['username']; ?>" readonly required>
        </div>
        <div class="col-md-4">
            <label for="email">Customer mail</label>
            <input class="form-control" type="email" id="email" name="visitor_email"
                   value="<?php echo $row['email']; ?>" placeholder="john.doe@email.com" readonly required>
        </div>
        <div class="col-md-4">
            <label for="phone">Customer Phone</label>
            <input class="form-control" type="tel" id="phone" name="visitor_phone" placeholder="07xxxxxx" required
                   pattern="((\+44(\s\(0\)\s|\s0\s|\s)?)|0)7\d{3}(\s)?\d{6}">
        </div>
        <hr>
        <div class="col-md-4">
            <label for="guests">Guests</label>
            <input onChange="calculatePrice()" type="number" id="guests" name="total_guests" min="1" max="50"
                   required class="form-control">

        </div>
        <div class="col-md-4">
            <label for="reservation_date">Reservation Date</label>
            <input class="form-control" id="dataoKontroll" onkeydown="return false" name="date" required>
        </div>
        <div class="col-md-4">
            <label for="package-selection">Package</label>
            <select class="form-control" onChange="calculatePrice()" id="package-selection"
                    name="package_preference" required>
                <option value="">Option for bundle</option>
                <option value="meal1">Bundle Option 1</option>
                <option value="meal2">Bundle Option 2</option>
                <option value="nobundle">No bundle</option>
            </select>
        </div>
        <div class="col-md-4">
            <label>Price</label>
            <p id="tot_amount" type="text" readonly></p>
        </div>
        <hr>
        <div class="col-md-12">
            <label for="message">Extra information</label>
            <textarea id="message" rows="4" cols="50" name="visitor_message"
                      placeholder="Important Detail or Message left" required="" class="form-control"
                      style="margin-top: 0px; margin-bottom: 0px; height: 244px;"></textarea>
        </div>
        <button type="submit" name="submit">Book Venue</button>
        <?php
        if (isset($_GET['success'])) {
            echo '<br>';
            echo '<p class="bg-success">Go to Manage Reservation to Pay Due Amount</p>';
        } else if (isset($_GET['error'])) {
            echo '<p class="bg-warning">The date selection is busy, please select another date or refer to Venue Availability</p>';
        }

        ?>
        </form>
    </div>
</div>
</div>
</body>
<script>
    function calculatePrice() {
        var price = 2899;
        var e = $("#package-selection :selected").val();
        var n = $("#guests").val();
        var guestnr = n * 5;
        console.log(n);
        if (e == 'meal2') {
            price += 200 + guestnr;

        } else if (e == 'meal1') {
            price += 700 + guestnr;

        } else if (e == 'nobundle') {
            price += guestnr;
        }
        var divfetch = $('#tot_amount');
        divfetch.text(price);
        console.log($("#package-selection :selected").val());
    }
</script>
</html>

