<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Yasin Zamani Konari">
    <title>CLASSIC VENUE</title>

    <link href="..\css\bootstrap.css" rel="stylesheet">
    <link href="..\css\style.css" rel="stylesheet">
    <script src="..\js\jquery.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
          integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
          crossorigin="anonymous"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
            integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
            crossorigin="anonymous"></script>
    <script src="../shared/js/scripts.js"></script>
</head>
<body>
<?php
session_start();
if ($_SESSION['type'] == 'user') {

} else {
    echo 'session not set :(!)';
    header("location:../signin.php?error=invalidSession");
}
?>


<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="list-group">


        <br>
        <a href="dashboard.php" class="list-group-item list-group-item-action">Book Venue</a>
        <a href="venueava.php" class="list-group-item list-group-item-action">Venue Availability</a>
        <a href="viewreservations.php" class="list-group-item list-group-item-action">Manage Reservation</a>
        <a href="upcomingevents.php" class="list-group-item list-group-item-action">Upcoming Events</a>
        <a href="myaccount.php" class="list-group-item list-group-item-action active">My Details</a>
        <a href="#" id="delete" name="delete" deleteaccount=<?php echo $_SESSION['email']; ?> class="list-group-item
           list-group-item-action">Delete Account</a>
        <a href="../shared/logout.php" class="list-group-item list-group-item-action">Log out</a>
    </div>

    <!-- Page Content -->
    <?php
    include '..\functions.php';
    $email = $_SESSION['email'];
    $conn = connection();
    $confirmation = confirmationNumber();
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $query = "SELECT * FROM users WHERE email =?;";
    $stmt = $conn->prepare($query);
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();
    ?>
    <div class="container">
        <h1 class="mt-1 text-">Changes Details</h1>
        <form class="row" action="changedetails.php" method="post">
            <div class="row">

                <div class="col-lg-3">
                    <label for="name">Your Name</label>
                    <input class="form-control" type="text" id="name" name="name"
                           value="<?php echo $row['username']; ?>" pattern="([A-zÀ-ž\s])+" minlength="5" maxlength="20"
                           required readonly required>
                </div>
                <div class="col-lg-3">
                    <label for="name">Customer Email</label>
                    <input class="form-control" type="email" id="email" name="email" value="<?php echo $row['email']; ?>"
                           readonly required placeholder="Email">
                </div>
            </div>
            <div class="col-lg-3">
                <label for="email">New Password</label>
                <input class="form-control" type="password" id="password" name="password" placeholder="New Password"
                       pattern="([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*" minlength="5" maxlength="20"
                       oninvalid="setCustomValidity('Password must include Letters and Numbers')"
                       oninput="setCustomValidity('')" required readonly>
                <div class="col-lg-5">
                    <button type="submit" name="submit" disabled>Save Changes</button>
                </div>
                <?php
                if (isset($_GET['success'])) {
                    echo '<br>';
                    echo '<p class="bg-success">Details Updated, Logging out to apply changes</p>';
                    header('Refresh: 2; url=http://localhost/myProject/shared/logout.php');
                } else if (isset($_GET['error'])) {
                    echo '<p class="bg-warning">Email Already Exist</p>';
                }


                ?>
                <label for="confirmchanges">Enable Fields</label>
                <input type="checkbox" id="confirmchanges" name="confirmchanges" value="Enable Changes">
            </div>
    </div>

    </form>


</div>
</div>
</div>
</body>
</html>

