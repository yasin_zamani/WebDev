<?php
/*
 * Deletes the customers account
 * Made by Yasin Zamani Konari
 */
include "../connection.php";
session_start();
switch ($_SESSION['type']) {
    case 'admin' or 'att':
        $conn = connection();
        $confirmation = $_GET['delete'];
        $sql = "DELETE from users WHERE email = ?;";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $confirmation);
        $stmt->execute();
        header("location:leave.php");
        break;
    default:
        echo 'invalid session';
        break;
}
