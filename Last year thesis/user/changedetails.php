<?php
/*
 * Changes the details of user with some error checking
 * Made by Yasin Zamani Konari
 */
include '../functions.php';
session_start();
if (isset($_POST['submit'])) {
    $user = $_POST['name'];
    $email = $_POST['email'];
    $pwd = $_POST['password'];
    if ($email == $_SESSION['email']) {
        header("location:myaccount.php?success=yes");
    } elseif (emailExists($email) !== false) {
        header("location:myaccount.php?error=emailExists");
        exit();
    } else {
        header("location:myaccount.php?success=yes");
    }
    UpdateDetails($user, $email, $pwd);
    updateReservation($email, $user);

}

?>
