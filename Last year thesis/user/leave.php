<?php
/*
 * When user deletes their account, this script will be executed.
 * Made by Yasin Zamani Konari
 */
echo 'Sorry for you to delete your account with us.
';
header('Refresh: 3; URL=../shared/logout.php');

