<!--
 Author : Yasin Zamani Konari
 email: yzk2@protonmail.com
 -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sign up Page">
    <meta name="author" content="Yasin Zamani Konari">

    <title>CLASSIC VENUE</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="shared/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>

</head>

<body>

<div class="container">
    <div class="header clearfix">
        <nav class="navbar navbar-expand-sm">
            <div class="container justify-content-end">
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse"
                        data-bs-target="#CollapseContent">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="CollapseContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0 nav-pills">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="aboutus.php">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="venue.php">Venue</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contactus.php">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="signup.php">Register</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signin.php">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <br>
    <h3 class="text-muted">CLASSIC VENUE</h3>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-5">
                <div class="card cardbox">
                    <div class="card-header">Welcome to the registration form</div>
                    <div class="card-body">
                        <form action="signup.inc.php" id="login-nav" method="post">
                            <div class="row">
                                <div class="col-lg">
                                    <label class="form-label">Full Name</label>
                                    <input type="text" id="reg_username" name="user_name" class="form-control"
                                           value="" placeholder="Full Name" pattern="([A-zÀ-ž\s])+" minlength="5"
                                           maxlength="20" required>
                                    <label class="form-label">E-mail Address</label>
                                    <input type="email" id="reg_useremail" name="user_email" class="form-control"
                                           value="" placeholder="Email" required>
                                    <label class="form-label">Password</label>
                                    <input type="password" id="reg_userpassword" name="user_password"
                                           class="form-control" placeholder="Password"
                                           pattern="([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*" minlength="5"
                                           maxlength="20" required>
                                    <label class="form-label">Repeat Password</label>
                                    <input type="password" id="reg_userpassword2" name="user_password2"
                                           class="form-control"
                                           placeholder="Repeat Password"
                                           pattern="([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*"
                                           minlength="5" maxlength="20"
                                           oninvalid="setCustomValidity('Password must include Letters and Numbers')"
                                           oninput="setCustomValidity('')" required>
                                    <br>
                                    <button id="reg_submit" name="submit" value="1" class="btn btn-block btn-primary">
                                        Create Account
                                    </button>
                                </div>


                                <!-- Error handler-->
                                <?php
                                if (isset($_GET['error'])) {
                                    if ($_GET['error'] == 'pwdmismatch') {
                                        echo '<p style="color:#ff0000;">Password is not matching with second password!</p>';
                                    } else if ($_GET['error'] == 'emailExists') {

                                        echo '<p style="color:red;">Email already exists!</p>';
                                    } else if ($_GET['error'] == 'sucess') {

                                        echo '<p style="color:green;">Sucessfully registered!</p>';
                                    }


                                }

                                ?>
                                <div class="bottom text-center">
                                    Are you user? <a href="signin.php"><b>Login</b></a>
                                </div>
                            </div>
                    </div>
                </div>
                <footer class="footer">
                    <p>© CLASSIC VENUE 2021</p>
                </footer>
            </div>


        </div>
    </div>

</body>
</html>
