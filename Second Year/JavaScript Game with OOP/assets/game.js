(function () {
    window.Start = {};
 /*
new object of the game.
*/ 
 var Game = Start.Game = function(ctx_1, xdimen, ydimen) {
    // initialize values
    // constants for our "Camera Views" having it constant so it is not changed. and these two values are pretty much accurate changing these values will cause the game to be not felt as a game.
    const  xy = 50;
    const  xz = 10;
    this.cameraHeight    = xy;
    this.cameraDepth     = xz;
    // the width of the road same as boundaries 
    this.roadWidth       = 160; 
    this.speed           = 0; 
    this.acceleration    = 0; 
    this.segments        = []; 
    // player positions
    this.playerX         = 0; 
    this.playerZ         = 0;
    // start a timer and we can calculate "Your Time"
    this.startTime       = new Date().getTime();
    // array of boundaries on side : future colision detection.
    this.OnSides     = [-160, 160];
	// the finish line more you put the longer the road will be
    this.numOfSegments   = 600;
    this.segmentLength   = 200;
    // distance of drawing on the road on screen 
    // the value between 50-100 is good the higher the more GPU power is needed.
    this.drawDistance    = 100;
    // set our functions parameters.
    this.ctx             = ctx_1;
    this.height          = ydimen;
    this.width           = xdimen;

   // preconfigured methods to be loaded on first time of game
    preconfigure.loadCar(this);
    preconfigure.generateSegments(this);
    preconfigure.greet();
    preconfigure.sendName(name);
	
  };
  // variables for our key movement.
  var 
  UP = 38,
  LEFT = 37,
  RIGHT = 39,
  DOWN = 40;

/*
 Key movements
 Basic key concepts. Move left,right and forward.
*/
  Game.prototype.keyPressed = function(e) {
	   //console.log(e.keyCode);
	   switch(e.keyCode) {
	   case UP:
	   
	   this.acceleration = 5.0;
	   break;
	   case LEFT:
	   this.dx = -1/55;
	   break;
	   case RIGHT:
	   this.dx = 1/55;
	   break;
	   };
  };
	/*
	when keys are !pressed decrease the current speed by 0.5.
	
	*/
  Game.prototype.keyNotPressed = function(e) {
	  if(event.which === e.keyCode.UP || event.which === UP) {
		  //console.log(e.keyCode);
		  this.acceleration = -.5; 
	  };
  };
 /*
 adjust the postion acording to the speed and a small check if it is on the sides.
 */
  Game.prototype.movementAdjust = function() {
	  // adjust it correctly so the car doesnt just fly away have to have a correct motion.
    var slowDown = this.onSides ? -5 : -this.speed/this.maxSpeed;
    if (this.speed >= this.maxSpeed) {
      this.speed += slowDown;
    } else {
      if (this.acceleration < 0 && this.speed <= 0) {
        this.speed = 0;
      } else {
        this.speed += this.acceleration;
      };
    };
	
    var xPos = this.playerX + this.speed*this.dx;
	// moving it left and right
    if (xPos > this.OnSides[0] && xPos < this.OnSides[1]) {
      this.playerX = xPos;
    }
	// straight forward
    this.maxSpeed = 120;  
    this.playerZ += this.speed;
  };
 /*
  this method draws our road 
 */

 
Game.prototype.Road = function() {
    var currentSegment = Math.floor(this.playerZ/200) % this.segments.length;
    for(i = 0 ; i < this.drawDistance ; i++) {
      n = (i + currentSegment) % this.segments.length;
	  //console.log(currentSegment);
      if (this.segments[n][0] > this.playerZ) {
        preconfigure.drawSegment(
          this,
          this.segments[n][0],
          this.segments[n][1],
          this.segments[n][2],
          this.segments[n][3]
        )
      }
    }
  };
 /*
 This prototype was based on time, where you need to collect X amount of coins in a time.
 basic timer.
 */
  Game.prototype.Timer = function() {
	
    this.ctx.font = "10px Helvetica";
    this.ctx.fillStyle = 'red';
    this.ctx.fillText("Your Time: " + (new Date().getTime() - this.startTime)/1000, 20, 10);
}

 
 /*
 our car sprite-image will be displayed on the center of screen and will be adjusted accordingly to the display.
 */
  Game.prototype.Car = function() {
   // constants so these values are not changed, if so the car will be displayed wrong.
    const a = 3.5;
    const b = 1.5;
    const c = 2.5;
    const d = 3;
    this.ctx.drawImage(
      this.car,
      this.width/a,
      this.height/b,
      this.width/c,
      this.height/d
    );
  };
 Game.prototype.Warning = function() {
  this.ctx.font = "15px Helvetica";
  this.ctx.fillStyle = '#000000';
  this.ctx.font = '36px Arial';
  this.ctx.textAlign = 'center';
  this.ctx.fillText('Game is not fully developed!',400,80);
}
 /*
 Render these methods on every 60th frame in the second.
 */
  Game.prototype.drawDisplay = function() {
    this.ctx.clearRect(0, 0, this.width, this.height);
    this.movementAdjust();
    this.Road();
    this.Car();
    this.Timer();
    //this.Warning();
  };
  Game.prototype.run = function() {
    // looping the game 60 frames in a second.
    this.gameLoop = setInterval(this.drawDisplay.bind(this), 1000/60);
  };

})();
