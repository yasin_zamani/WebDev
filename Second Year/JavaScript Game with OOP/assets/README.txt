General information about the game and reused code and files

#Car Game
This game is based on 80's retro car racing game and i got this idea from this tutorial made by : "javidx9"  but the code is written in C++
So I had to be creativite to make it to JavaScript code 
C++ code: https://www.youtube.com/watch?v=KkMZI5Jbf18
His Channel : https://www.youtube.com/channel/UC-yuWVUplUJZvieEligKBkA/featured
-I have done plenty of research before implementing this, it is not a complete game,
but it has the basic of what you can call it a game.
In the summer ill finish this game one as my projects.

# Controls

Your controls for this game are :

-Arrow up to accelerate the car,
-Arrow Left to move left
-Arrow Right to move right
-Not pressing any buttons will decrease your speed and stand.

# Rules

	No rules for now :D



# Research

-Understanding world view and projection : http://www.codinglabs.net/article_world_view_projection_matrix.aspx

-Understanding model view projection : https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/WebGL_model_view_projection

-Refreshing some basic maths from Khan Academy : Khanacademy

- Usage of ECMAScript 5 : https://www.w3schools.com/js/js_es5.asp

- Tutorial on JQuery : https://www.w3schools.com/jquery/

- Polygon : https://www.youtube.com/watch?v=3kICmEJ-xyo

- More Polygon : https://www.youtube.com/watch?v=Newm6jnp7T0

- Push on Array : https://www.w3schools.com/jsref/jsref_push.asp

- Binding : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Function/bind

# Sources, libraries and code reuse

-JQuery lib : https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js

-photos\lambo.png : https://boingboing.net/2013/01/17/tell-us-about-the-games-you-al.html

-sending name to db and retriving from db is used from part2

-greeting user and storing it to db used from part2.

- some part of Polygon idea used : https://github.com/frankarendpoth/frankarendpoth.github.io/blob/master/content/pop-vlog/javascript/2018/024-polygon/polygon.html

- some ideas from this youtube tutorial : https://www.youtube.com/watch?v=Newm6jnp7T0

# Directory files

-Folder ./assets the main code of the game is stored here such as:
	./assets/add_to_leaderboard.php adds name to leaderboard
	./assets/connection.php/ connects to database and my login information to db.
	./assets/game.js/ Main game file
	./assets/preconfigure.js/ Preconfigure settings before executing them in game.js
	./assets/read_data_from_leaderboard.php/ reads the names of users from database and displays on index.html who played my game
	./assets/README.txt/ Current File
	./assets/css/style.css/ Styling of the page
	./photos/lambo.png/ our car sprite
	./index.html/ our main page where we load preconfigure the game file and start and playing it.
	./about.html/ sources used and crediting
	./help.html/ help and information about the game.
 







	

