(function () {
	// define preconfigure.
	window.preconfigure = {};
	// reused function from part 2

	preconfigure.greet = function() {
	name = localStorage.getItem("namez");
	if (name == null || name == "null"){
	alert("Hi, Stranger!");
	name = prompt("What is your name?");
	localStorage.setItem("namez", name);
	} else {
	console.log("Hi, " + name + "!");
	}  
	};
	// reused function from part 2

	preconfigure.sendName = function(name) {
	var request;
	request = new XMLHttpRequest();
	var variables = "name=" + name;
	request.open("POST", "./assets/add_to_leaderboard.php", true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.send(variables); 
	};
	
	// reused function from part 2
	preconfigure.loadNames = function() {
	var request = new XMLHttpRequest();
	request.open("POST", "./assets/read_data_from_leaderboard.php", true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.onreadystatechange = function() {
	if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
	var displayData = request.responseText;
	document.getElementById("records").innerHTML = displayData;
	}
	}
	request.send(null);
	};
	
	preconfigure.loadCar = function (game) {
	game.car = new Image();
	game.car.src = "./photos/lambo.png";
	};


	// basic function for drawing a polygon, when drawing a polygon it will ask for a polygon's value xy + the colour.
        preconfigure.polygon = function (ctx, xOneValue, yOneValue, xTwoValue, yTwoValue, xThreeValue, yThreeValue, xFourValue, yFourValue, colour) {
        ctx.fillStyle = colour;
        ctx.beginPath();
        ctx.moveTo(xOneValue, yOneValue);
        ctx.lineTo(xTwoValue, yTwoValue);
        ctx.lineTo(xThreeValue, yThreeValue);
        ctx.lineTo(xFourValue, yFourValue);
        ctx.closePath();
        ctx.fill();
    };



	// styling generate for segments for the road(White color in the middle)
	preconfigure.generateSegments = function (game) {
		// for loop to generate the "stripes" on the road. and the length of the road(finishing point)
	for (var n = 0; n < game.numOfSegments; n++) {
			// avgang mellom hvite stripen i midten og vegen. // verdi 3 er bra
			// gj�r om til engelsk etterp�.
			// variablene m� defineres i game.js som "segmentLength" og numOfSegments.
			var verdi = 3;
	var colour = Math.floor(n) % verdi ? "#696969" : "white";
	//https://www.w3schools.com/jsref/jsref_push.asp    
	// each segement is pushed to the array with the color for this case var colour.         
	game.segments.push([
	n * game.segmentLength,
	(n + 1) * game.segmentLength,
	colour
	]);
	}
	;
	};

	 // read README.txt
	// some ideas from that pages is applied here.
	// this function is important, without this we are not able to get our "2d" values to our screen.
	// and pointing the camera angles correctly on screen display.
	
	preconfigure.CameraView = function (game, worldX, worldY, worldZ, objectWidth) {
	var angleX = worldX - game.playerX;
	var angleY = worldY - game.cameraHeight;
	var angleZ = worldZ - game.playerZ;
	var scaling = game.cameraDepth / angleZ;
	var xValue = Math.round(game.width / 2 + scaling * angleX * game.width / 2);
	var yValue = Math.round(game.height / 2 - scaling * angleY * game.height / 2);
	var wValue = Math.round(scaling * objectWidth * game.width / 2);
	return [
	xValue,
	yValue,
	wValue
	];
	};

	// drawing of  segment road and the artifical grass. if no colour is given the default colour of seagment will be assigned.
	preconfigure.drawSegment = function (game, start, end, colour) {
	// get starting points 
	var startP = preconfigure.CameraView(game, 0, 0, start, game.roadWidth);
	var endP = preconfigure.CameraView(game, 0, 0, end, game.roadWidth);
	// define them
	var x1 = startP[0];
	var y1 = startP[1];
	var w1 = startP[2];
	var x2 = endP[0];
	var y2 = endP[1];
	var w2 = endP[2];
        // grass one left
	preconfigure.polygon(game.ctx, 0, y1, x1-w1, y1, x2-w2, y2, 0, y2,"green");
	// grass right side
	preconfigure.polygon(game.ctx, game.width, y1, x1+w1, y1, x2+w2, y2, game.width, y2,"green");
	preconfigure.polygon(game.ctx, x1 - w1, y1, x1 + w1, y1, x2 + w2, y2, x2 - w2, y2, "#694563");    
	preconfigure.polygon(game.ctx, x1-(w1/25), y1, x1+(w1/25), y1, x2+(w2/25), y2, x2-(w2/25), y2, colour);
	};
	}
	());
