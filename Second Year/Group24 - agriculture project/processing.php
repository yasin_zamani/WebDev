<?php

    $parentId = 0;

    #Insert 'choiceText' and will return the level
    function getNextLevel($name){
      $db = new SQLite3('agri.sqlite');
      $results = $db->query('SELECT * FROM LevelItem');

      while($row = $results->fetchArray()){
        if ($name == $row['choiceText']) {
          return $row['nextLevelQues'];
        }
      }
    }

    #Insert 'choiceText' and will return the id
    function getId($name){
      $db = new SQLite3('agri.sqlite');
      $results = $db->query('SELECT * FROM LevelItem');

      while($row = $results->fetchArray()){
        if ($name == $row['choiceText']) {
          return $row['id'];
        }
      }
    }

    #Insert 'choiceText' and will return the parentId
    function getParentId($name){
      $db = new SQLite3('agri.sqlite');
      $results = $db->query('SELECT * FROM LevelItem');

      while($row = $results->fetchArray()){
        if ($name == $row['choiceText']) {
          return $row['parentId'];
        }
      }
    }

    function printSubCat($id, $level, $parent){
        # Check what stage the content is being pulled from
        if($level == 0){
          #echo $id;
            $db = new SQLite3('agri.sqlite');
            $results = $db->query("SELECT * FROM LevelItem WHERE parentId='$id'");

            while ($row = $results->fetchArray()) {
              echo "<p>";
              echo "<a href='LevelThree.php?".$row['choiceText']."'>".$row['choiceText']."</a>";
              echo "</p>";
              echo nl2br("\n");
           }
         }else{
           # Pulls previous page URL
           $url = $_SERVER['HTTP_REFERER'];
           $value = parse_url($url);
           $urlquery = explode('.',$value['query']);
           $db = new SQLite3('agri.sqlite');
           $results = $db->query("SELECT * FROM LevelItem WHERE choiceText='$urlquery[0]'");

           while ($row = $results->fetchArray()) {
             $newparentID = $row['id'];
           }

           $url = urldecode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
           $value = parse_url($url);
           $urlquery = explode('.',$value['query']);

           $results = $db->query("SELECT * FROM LevelItem WHERE parentId='$newparentID'");
           while ($row = $results->fetchArray()) {
             if($urlquery[0] == $row['choiceText']){
               $newID = $row['id'];
             }
           }
           #echo $newID;

           $results = $db->query("SELECT * FROM LevelItem WHERE parentId='$newID'");
           while ($row = $results->fetchArray()) {
             echo "<p>";
             echo $row['choiceText'];
             echo "</p>";

             echo "<p>";
             echo $row['answerText'];
             echo "</p>";
             echo nl2br("\n");
           }
         }
     }

     # Ask Question page - NEEDS CHANGING -
     function questions(){
       echo "<h2> Ask Question </h2>";
       echo "
       <!-- ---- James G's Code -->
       <div class=form>
         <form name='form1' action='submissionComplete.php' method='POST'>
           E-mail: <input type='text' name='Email' placeholder='Your Email Address' required><br>
           Question: <input type='text' name='Question' placeholder='Your Question' required><br>
           <input type='submit' id='confirmModal' disabled='true'>
         </form>
       </div>
       <!-- ---- -->";
       # ----

       /* WIP - Modal for successful datbase entry
       echo "
       <div id='modal' class='modal'>
         <div class='m-content'>
           <!-- ---- Mats code -->
           <h1> Question Submitted!</h1>
           <p> Your question has successfully been submitted. </p> <p> An email will follow shortly once your question has been answered.</p>
           <!-- ---- -->
         </div>
       </div>";
       */

       echo "<h3 onclick=".'back()'.">Go Back</h3>";

     }

     function sendData(){
       # FIX 'UNIQUE Constraint failed error' - Question matches question in database -
       if(isset($_POST['Question']) && isset($_POST['Email'])){
         $db = new SQLite3('./login/newQuestions.sqlite');
         $question = $_POST['Question'];
         $email = $_POST['Email'];
         $db->exec("INSERT INTO newQuestions (email, question, identifier) VALUES ('$email', '$question',0)");
         #echo $_POST['Question'];
         #echo $_POST['Email'];
         $db->close();
       }
     }

    # Dynamic page content
    function check_menu(){
      # Pulls CURRENT URL & decodes (%20 == ' ')
      $url = urldecode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
      # Converts URL into ARRAY elements
      $value = parse_url($url); # $values = parse_url($url, PHP_URL_QUERY);
      # Breaks up array into chunks, pulling the 'query' (The part on the URL past '?')
      $urlquery = explode('.',$value['query']);
      #echo $$urlquery[0];
      $number = getParentId($urlquery[0]);
      $db = new SQLite3('agri.sqlite');

      $results = $db->query("SELECT * FROM LevelItem WHERE parentId='$number'");

      while($row = $results->fetchArray()){
        # Check if page and content matches
        if ($urlquery[0] == $row['choiceText']) {
          echo "<h2>".$row['choiceText']."</h2>";
          echo "<h3 onclick=".'back()'.">Go Back</h3>";
          echo nl2br("\n");
          printSubCat(getId($row['choiceText']), getNextLevel($row['choiceText']), getParentId($row['choiceText']));
        }
      }
    }

    # Main Heading on each page
    function title(){
      global $parentId;
      $db = new SQLite3('agri.sqlite');
      $results = $db->query("SELECT * FROM LevelItem WHERE parentId='$parentId'");

      while ($row = $results->fetchArray()) {
        echo $row['choiceText'];
      }
      #$db = null;
    }

    # Main Page headings
    function main_menus(){
      global $parentId;
      $parentId = getId($parentId);
      $db = new SQLite3('agri.sqlite');

      $results = $db->query("SELECT * FROM LevelItem WHERE parentId='$parentId'");
      while ($row = $results->fetchArray()) {
        echo "<p>";
        echo "<a href='LevelTwo.php?".$row['choiceText']."'>".$row['choiceText']."</a>";
        echo "</p>";
        echo nl2br("\n");
      }
      echo "<p> <a href='AboutUs.html'> About Us </a> </p>";
    }
 ?>
