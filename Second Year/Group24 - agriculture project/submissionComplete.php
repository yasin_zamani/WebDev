<?php require 'processing.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>

<title>Question Submitted</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv = "refresh" content = "4; url = index.php" />
<link rel="stylesheet" href="main.css">

</head>

<body>
<?php sendData(); ?>
<h1> Question Submitted!</h1>
<p> Your question has successfully been submitted, an email will follow shortly once your question has been answered.</p>

</body>
</html>
