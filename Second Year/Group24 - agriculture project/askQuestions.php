<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="main.css">
  <title>Farmer Advice System</title>
</head>
<body>

  <h1> Farmer Advice System </h1>

  <div class="menu-container">
    <h2> Ask Question </h2>
    <!-- ---- James G's Code -->
    <div class=form>
      <form name='form1' action='submissionComplete.php' method='POST'>
        E-mail: <input type='text' name='Email' placeholder='Your Email Address' required><br>
        Question: <input type='text' name='Question' placeholder='Your Question' required><br>
        <input type='submit' id='confirmModal' disabled='true'>
	<span id='greater'>Question must be greater than 2 char.</span>
      </form>
    </div>
    <!-- ---- --><h3 onclick=back()>Go Back</h3></div>

    <script>
    function back() {
      window.history.back();
    }
    let submitButton = document.getElementById("confirmModal");
    function validateEmail(email){
      var re = /\S+@\S+\.\S+/;
      return re.test(email);
    }
    function check(){
      let formEmail = document.forms["form1"]["Email"].value;
      let formQuestion = document.forms["form1"]["Question"].value;
      if(submitButton.disabled = true){
        // Check form valid email & question length is greater than 3 stringlength
        //to prevent spams
        //console.log(validateEmail(formEmail));
	document.getElementById("greater").style.display = 'block';
        if(validateEmail(formEmail) == true & formQuestion.length > 2) {
          console.log('valid email & form length');
            // enable button to submit
            submitButton.disabled = false;
	    document.getElementById("greater").style.display = 'none';
	   
        } {
          //Infinite looping function
          setTimeout(check, 1000);
        }

      }
    }
    check();
    </script>
  </body>
  </html>
