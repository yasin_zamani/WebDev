<?php require 'processing.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="main.css">
  <title>Farmer Advice System</title>
</head>
<body>

<h1> <?php title(); ?> </h1>

<div class="menu-container">
   <?php check_menu(); ?>
   <p><a href='askQuestions.php'>Ask Question</a></p>
</div>

<script>
function back() {
  window.history.back();
}
</script>

</body>
</html>
