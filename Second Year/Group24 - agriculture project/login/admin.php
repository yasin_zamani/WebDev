<?php require 'process.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="admin_main.css">
  <title>Document</title>
</head>
<body>
  <?php adminPage(); ?>

  <script>
    var Questions = document.getElementById('questions');
    var Answers = document.getElementById('answers');
    var Answer = document.getElementById('answers');
    function showQuestions(){
      Questions.style.display = "block";
      Answers.style.display = "none";
    }

    function showAnswers(){
      Questions.style.display = "none";
      Answers.style.display = "block";
    }
  </script>
</body>
</html>
