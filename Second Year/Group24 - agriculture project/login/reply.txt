<?php
session_start();

//grab question id based on non answered questions
$id = $_GET['id'];
$db = new SQLite3('newQuestions.sqlite');

// select query based on the questionID

$result = $db->query("SELECT * FROM newQuestions where QID = '$id'");
// admin check, if session is valid display the page if not redirect to login page.
if(isset($_SESSION["username"])) {
    
  if(isset($_POST['submit'])) { // will execute this line only if submit button is pressed
    
    $answer = $_POST['msg'];
    
    if($db) { // if db exist
        
        // get single row of the message & set the answer based on the form. 
        // lastly send the question and answer to the persons mail. 
        
      $row = $result->fetchArray();
      $db->query("UPDATE newQuestions set answer= '$answer', identifier='1' WHERE QID = '$id'");
      $toEmail="$row[1]";
      $subject="Question Replied!";
      $body='
      <h4>Question:</h4><p>'.$row[2].'</p>
      <h4>Answer:</h4><p>'.$answer.'</p>
      ';
      $question = $row[2];
      $headers="MIME-Version:1.0"."\r\n";
      $headers.="Content-Type:text/html:charset=UTF-8"."\r\n";
      mail($toEmail,$subject,$body,$headers);
      header("Location:admin.php");
    }

  }

} else {
  echo "no access";
  header("Location:admin.php");

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1"/>
  <link rel="stylesheet" type="text/css" href="admin_main.css">
</head>
<body>
  <form class = "log" method="POST" action="">
    <p> Reply to question</p>
    <?php
    $row = $result->fetchArray();
    echo "<p>$row[2]</p>";
    ?>
    <textarea rows="10" cols="50" wrap="physical" name="msg" required></textarea>
    <br>
    <button name="submit"> Submit</button>
  </form>
</body>
</html>
