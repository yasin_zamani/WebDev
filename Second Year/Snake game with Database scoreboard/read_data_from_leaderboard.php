<!DOCTYPE html>
<html>
	<title>Leaderboard data</title>
		<body>
		
		<table table align = "center" style="font-size:50px" border = "2" cellpadding="20">
			<style>
			h2 {
			 text-align:center;
			}
			</style>
			<h2>Leaderboard</h2>
			<tr>
				<th>Name</th>
				<th>Score</th>
			</tr>
			<?php 
			include 'connection.php';
			$dbconn = connection();
			$score = pg_query($dbconn, "select * from leaderboard ORDER by score DESC;");
			while($rows=pg_fetch_assoc($score))
			{
			$c_row = $rows['name'];
			$cx_row =$rows['score'];
			$name=htmlspecialchars($c_row,ENT_QUOTES);
			$scorez=htmlspecialchars($cx_row,ENT_QUOTES); 

			echo "
			<tr>
				<td>$name</td>
				<td>$scorez</td>
			</tr>
			";
			}
			//frees the memory 
			pg_free_result($score);
			// close connection
			closeCon($dbconn);
			?>			
			</table>
			</body>
			</html>
