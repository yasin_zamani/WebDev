 /**
  * Sends data to our database with the help of AJAX
  * @param  {string} our name holder to be sent
  * @param  {number} our score to be sent
  *
  */
//https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest

var request;
function sendScore(name, score) {
    request = new XMLHttpRequest();
	// set variables
    var variables = "name=" + name + "&score=" + score;
 //open a request as Post and direct our php page without refreshing.
    request.open("POST", "add_to_leaderboard.php", true);
    // Set content type header information for sending url encoded variables in the request
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    //Here we send our request to our prepared PHP which takes in values as _POST more secure(possibility of tampering).
    request.send(variables); 
}

 /**
  * Loads data from read_data.php to our index.html page
  */

function loadDatabase() {
    var request = new XMLHttpRequest();
    request.open("POST", "read_data_from_leaderboard.php", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.onreadystatechange = function() {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                var displayData = request.responseText;
                document.getElementById("records").innerHTML = displayData;
            }
        }
        request.send(null); // ajax wouldnt be executed without this.
    	
}
/**
* greet user when they first time visit the page
*/
  function greet(){
    name = localStorage.getItem("name");
    if (name == null || name == "null"){
      alert("Hi, Stranger!");
      name = prompt("What is your name?");
      localStorage.setItem("name", name);
    } else {
      //console.log("Hi, " + name + "!");
    } // end greet
  } // end function  
  

 

var once = false;
// greet user ask username then store the name to the localDB in our browser 
// which will be accessed via php and postgresql to avoid duplicates.
greet();

// game over check 
var gameover = false;

// load images
const foodImg = new Image();
foodImg.src = "img/food.png";
const snakeHead = new Image();
snakeHead.src = "img/snakehatup.png";
const snakeDown = new Image();
snakeDown.src = "img/snakehatdown.png";
const snakeLeft = new Image();
snakeLeft.src = "img/snakehatleft.png";
const snakeRight = new Image();
snakeRight.src = "img/snakehatright.png";

// audio for eating
// optional can be turned off and on in settings.
let eat = new Audio();
eat.src = "eat.wav";
let bgmusic = new Audio();
bgmusic.src = "bgmusic.wav";


// variables for our randompositions for our canvas 

// experimental way of saving colour in JSON.
  var randpox;
  var randpoy;
  var colour;
var

/**
 * Constats
 */
COLS = 26,
ROWS = 26,

EMPTY = 0,
SNAKE = 1,
FRUIT = 2,
FRUITS = 4,

LEFT  = 0,
UP    = 1,
RIGHT = 2,
DOWN  = 3,

KEY_LEFT  = 37,
KEYA 	  = 86,
KEYS 	  = 83,
KEYW 	  = 87,
KEYD	  = 68,
KEY_UP    = 38,
KEY_RIGHT = 39,
KEY_DOWN  = 40,


/**
 * Game objects
 */
canvas,   /* HTMLCanvas */
ctx,   /* CanvasRenderingContext2d */
keystate, /* Object, used for keyboard inputs */
frames,  /* number, used for animation */
mouseX,
mouseY,
score,
scorez;   /* number, keep track of the player score */


/**
 * Grid datastructor, usefull in games where the game world is
 * confined in absolute sized chunks of data or information.
 * 
 * @type {Object}
 */
grid = {

 width: null,  /* number, the number of columns */
 height: null, /* number, the number of rows */
 _grid: null,  /* Array<any>, data representation */

 /**
  * Initiate and fill a c x r grid with the value of d
  * @param  {any}    d default value to fill with
  * @param  {number} c number of columns
  * @param  {number} r number of rows
  */
 init: function(d, c, r) {
  this.width = c;
  this.height = r;

  this._grid = [];
  for (var x=0; x < c; x++) {
   this._grid.push([]);
   for (var y=0; y < r; y++) {
    this._grid[x].push(d);
   }
  }
 },

 /**
  * Set the value of the grid cell at (x, y)
  * 
  * @param {any}    val what to set
  * @param {number} x   the x-coordinate
  * @param {number} y   the y-coordinate
  */
 set: function(val, x, y) {
  this._grid[x][y] = val;
 },

 /**
  * Get the value of the cell at (x, y)
  * 
  * @param  {number} x the x-coordinate
  * @param  {number} y the y-coordinate
  * @return {any}   the value at the cell
  */
 get: function(x, y) {
  return this._grid[x][y];
 }
}

/**
 * The snake, works as a queue (FIFO, first in first out) of data
 * with all the current positions in the grid with the snake id
 * 
 * @type {Object}
 */
snake = {

 direction: null, /* number, the direction */
 last: null,   /* Object, pointer to the last element in
      the queue */
 _queue: null,  /* Array<number>, data representation*/

 /**
  * Clears the queue and sets the start position and direction
  * 
  * @param  {number} d start direction
  * @param  {number} x start x-coordinate
  * @param  {number} y start y-coordinate
  */
 init: function(d, x, y) {
  this.direction = d;

  this._queue = [];
  this.insert(x, y);
 },

 /**
  * Adds an element to the queue
  * 
  * @param  {number} x x-coordinate
  * @param  {number} y y-coordinate
  */
 insert: function(x, y) {
  // unshift prepends an element to an array
  this._queue.unshift({x:x, y:y});
  this.last = this._queue[0];
 },

 /**
  * Removes and returns the first element in the queue.
  * 
  * @return {Object} the first element
  */
 remove: function() {
  // pop returns the last element of an array
  return this._queue.pop();
 }
};

/**
 * Set a food id at a random free cell in the grid
 * Then we do a multiplication to place the canvas image correctly by the random points in the grid cell.
 */
function setFood() {
 var empty = [];
 // iterate through the grid and find all empty cells
 for (var x=0; x < grid.width; x++) {
  for (var y=0; y < grid.height; y++) {
   if (grid.get(x, y) === EMPTY) {
    empty.push({x:x, y:y});
   }
  }
 }
 // chooses a random cell
 var randpos = empty[Math.round(Math.random()*(empty.length - 1))];
 grid.set(FRUITS,randpos.x,randpos.y);
 
 var randpo = empty[Math.round(Math.random()*(empty.length - 1))];
  // since our sprite cannot be used as the random integers in the array, we have to count how many rows and colums and multiply by it.
  randomx = (randpos.x * 19.8 + 0);
  randomy = (randpos.y * 20 - 13);
 
}
// for fun... 
// we add 1k score to our current score random and is invis. pure luck based.
function cheatingPoint() {
 var empty = [];
 // iterate through the grid and find all empty cells
 for (var x=0; x < grid.width; x++) {
  for (var y=0; y < grid.height; y++) {
   if (grid.get(x, y) === EMPTY) {
    empty.push({x:x, y:y});
   }
  }
 }
 // chooses a random cell
 var randpo = empty[Math.round(Math.random()*(empty.length - 1))];
 grid.set(FRUIT,randpo.x,randpo.y);
  // since our sprite cannot be used as the random integers in the array, we have to count how many rows and colums and multiply by it.
  //console.log("Setting a random cheating point! in the grid x - y  : " + randpo.x + "\n" + randpo.y);

  
}



/**
 * Starts the game
 */
// our background music function
function backgroundmusic() {
	 var Choice =  document.getElementById("sound").value;
	 if(Choice == "on") {
		bgmusic.play(); 
		//console.log("Sound is on");
	 } else {
		bgmusic.pause();
		 //console.log("Sound is off");
 }} 

function main() {
	backgroundmusic();
	// load our database when we click on Draw settings.
	loadDatabase();
	
	// create and initiate the canvas element
	frames = 0;
	keystate = {};
	// keeps track of the keybourd input
	document.addEventListener("keydown", function(evt) {
		keystate[evt.keyCode] = true;
	});
	document.addEventListener("keyup", function(evt) {
		delete keystate[evt.keyCode];
	});
	
	// intatiate game objects and starts the game loop
	if(gameover == true) {
		; // do nothing.
		  // game over? stop the game. pretty much annoying for it to auto restart.
		
	} else {
	init();
	loop();
	}

}

// classic menu for our game when the javascript is loaded.
function menu () {
	canvas = document.createElement("canvas");
	canvas.width = COLS*20;
	canvas.height = ROWS*20;
	ctx = canvas.getContext("2d");
	// add the canvas element to the body of the document
	document.body.appendChild(canvas);
	// sets an base font for bigger score display
	
  ctx.font = "15px Helvetica";
  ctx.fillStyle = '#000000';
  ctx.font = '36px Arial';
  ctx.textAlign = 'center';
  ctx.fillText('Classic Snake!', canvas.width / 2, canvas.height / 4);
  ctx.font = '24px Arial';
  ctx.fillText('Click to Start', canvas.width / 2, canvas.height / 2);
  ctx.font = '18px Arial'
  ctx.fillText('Use the arrow keys to move', canvas.width / 2, (canvas.height / 4) * 3);
  ctx.fillText('Speed is changeable in Settings tab', canvas.width / 2, (canvas.height / 3) * 2);
  // Start the game on a click
  canvas.addEventListener('click', startGame);
}

// start the game.
function startGame() {
  // Stop listening for click events
  canvas.removeEventListener('click', main);
  // load main
  gameover=false;
  loadDatabase();
  main();
}
// remove everything and make it blank
function erase() {
  ctx.fillStyle = '#ede9ce';
  ctx.fillRect(0, 0, 600, 600);
}
// our end Game when we die we save the score to our local db and we compare if the current score > the previous alert user with a box
// didn't completly finished the fucntion due to my part 3.
function endGame() {
  // Display the final score
  erase();
  ctx.fillStyle = '#000000';
  ctx.font = '24px Arial';
  ctx.textAlign = 'center';
  ctx.fillText('Final Score: ' + score, canvas.width / 2, canvas.height / 2);
  ctx.fillText('Click to Restart', canvas.width / 2, (canvas.height / 4) * 3);
  canvas.addEventListener('click', startGame);
  gameover=true;
  //recordScore();
}

/**
 * Resets and inits game objects
 */

function init() {

	score = 0;
	scorez = 0;

	grid.init(EMPTY, COLS, ROWS);

	var sp = {x:Math.floor(COLS/2), y:ROWS-1};
	snake.init(UP, sp.x, sp.y);
	grid.set(SNAKE, sp.x, sp.y);

	setFood();
	
	
	
}
// draw game over. and send the score to our postgresql.
function gameoverz() {
	draw_gameover()
	//console.log("GAME OVER ... SENDING DATA TO DB");
	sendScore(name, score);
	}
	
	

// Show the start menu
// restarting the game my old function before getting more experienced with javascript.

  function draw_gameover() {
	  ctx.fillStyle = 'red';
	  ctx.font = "30px Arial";
	  ctx.fillText("GAME OVER", 150, 250);
	  ctx.fillText("Final Score: " + score, 180, 300);
	  ctx.globalCompositeOperation = 'destination-over';
	  //console.log("Restarting in 2 sec..");
	  function timedRefresh(timeoutPeriod) {
	setTimeout("location.reload(true);",timeoutPeriod);
}

window.onload = timedRefresh(5000);
  }
  


/**
 * The game loop function, used for game updates and rendering
 */
function loop() {
	if(gameover == true) { // game over check if it is over DO NOTHING!
		;	
	}
	else {
		draw();
		update();
		window.requestAnimationFrame(loop, canvas);
	}
		
	// When ready to redraw the canvas call the loop function
	// first. Runs about 60 frames a second
	
}

/**
 * Updates the game logic
 */
function update() {
  var nx = snake.last.x;
  var ny = snake.last.y;

 frames++;
 // changing direction of the snake depending on which keys
 // that are pressed
 if (keystate[KEY_LEFT] && snake.direction !== RIGHT) {
  snake.direction = LEFT;
 }
 if (keystate[KEY_UP] && snake.direction !== DOWN) {
  snake.direction = UP;
 }
 if (keystate[KEY_RIGHT] && snake.direction !== LEFT) {
  snake.direction = RIGHT; 
 }
 if (keystate[KEY_DOWN] && snake.direction !== UP) {
  snake.direction = DOWN;
 }
// checking direction of snake then we apply the correct sprite/photo for the movement!
  if(snake.direction == LEFT) {
ctx.drawImage(snakeLeft,nx*19.8 + 0,ny*20-13);

} else if(snake.direction == RIGHT) {
ctx.drawImage(snakeRight,nx*19.8 + 0,ny*20-13);
} else if(snake.direction == DOWN) {
  ctx.drawImage(snakeDown,nx*19.8 + 0,ny*20-13);
} else {
 ctx.drawImage(snakeHead,nx*19.8 + 0,ny*20-13);
}

var speed =  document.getElementById("myRange").value;

	
 // each five frames update the game state.
 if (frames%speed=== 0) {
  // pop the last element from the snake queue i.e. the
  // head
  // updates the position depending on the snake direction
	
  switch (snake.direction) {
   case LEFT:
    nx--;
    break;
   case UP:
    ny--;
    break;
   case RIGHT:
    nx++;
    break;
   case DOWN:
    ny++;
    break;
  }


  // checks all gameover conditions
		if (0 > nx || nx > grid.width-1  ||
			0 > ny || ny > grid.height-1 ||
			grid.get(nx, ny) === SNAKE) {
				gameover = true;
				once = false;
				endGame();
				recordScore();
				// send score to database (There are checks of user, and score).
				// a user cannot be inserted multiple times to the list, it violates the first normal form!
				// if the user exist update the score only! only if they beat their previous high score.
				sendScore(name,score);
		}	// small javascript function to alert user if they beat their previous score..
			// risky, sendScore function sends the value from score a user can change this value from the local db.
	function recordScore() {
            var highScore = localStorage.scorez;
	    //console.log(highScore);
            if (score > highScore) {
                alert('You have beaten your previous high score, which was : ' + highScore + '\n' + 'Current HighScore is : ' +  + score + ' it will be updated to the database!');
                localStorage.setItem('scorez', scorez);
            }
        }


// draw the cheating point once in the game. (When you die it spawns again in the random intervals 10000ms - 500ms)		
function food() {
if(!once) {
	var count = 0;
	var abc = Math.floor(Math.random() * (10000 - 500)) + 500;
	//console.log(abc);
	//console.log(once);
	setTimeout(function() {cheatingPoint();}, abc);
	once = true;
 }
 }
// sound function
// checks what user chose from the index.html if Sound on or off 
// depending on that it will do what it is desired to do.	
 function sound() {
	 var Choice =  document.getElementById("sound").value;
	 if(Choice == "on") {
		eat.play(); 
		//console.log("Sound is on");
	 } else {
		 //console.log("Sound is off");
 }} 
	var abc = grid.get(nx,ny);
  // check wheter the new position are on the fruit item
  if (abc === FRUITS) {
	  
	 sound(); 
   // increment the score and sets a new fruit position
   score += 5;
   // our experimental localDB score check alert, which i had no time to fix.
   scorez += 5;
   setFood();
   if(food()){
	   once = true;
   }
  } else {
   // take out the first item from the snake queue i.e
   // the tail and remove id from grid
   var tail = snake.remove();
   grid.set(EMPTY, tail.x, tail.y);
  }
  // add a snake id at the new position and append it to 
  // the snake queue
  grid.set(SNAKE, nx, ny);
  snake.insert(nx, ny);
 }
 
 if(abc === FRUIT) {
  score += 1000; 
  grid.set(SNAKE, nx, ny);
  snake.insert(nx, ny);
 }
 }





 
// random color on the snake ? why not.

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
// color choice by the sure
// here i had alot of if else statment, i trim the code by adding the choice value to our local DB
//from there we assign the color
// experimenting with JSON here too. 
function verifycolor() {
	var Choice =  document.getElementById("Choice").value;
	var red = "red";
	var green = "green";
	var random = "random";
	var blue = "blue";
	var storedchoice;
	var Store = {
		'color' : Choice
	}

	storedchoice = localStorage.setItem("colour", Choice);
	localStorage.setItem('Store', JSON.stringify(Store));
	var result = JSON.parse(localStorage.getItem('Store'));
    var CurrentColor = localStorage.colour;
	
	if(CurrentColor == random) {
	ctx.fillStyle = getRandomColor();
	} else {
		ctx.fillStyle = CurrentColor;
}
}


  
  

/**
 * Render the grid to the canvas.
 */

function draw() {	
	
 // calculate tile-width and -height
 var tw = canvas.width/grid.width;
 var th = canvas.height/grid.height;
 // iterate through the grid and draw all cells
 
 for (var x=0; x < grid.width; x++) {
	// draw our apple on the game
	ctx.drawImage(foodImg,randomx,randomy);
  for (var y=0; y < grid.height; y++) {
   // sets the fillstyle depending on the id of
   // each cell
// draw our apple on the game
// why twice? sometimes the apple doesnt bother spawning.
   ctx.drawImage(foodImg,randomx,randomy);
   
   switch (grid.get(x, y)) {
    case EMPTY:
     ctx.fillStyle = "#ede9ce";
     break;
    case SNAKE: 
	verifycolor();
     break;
    case FRUIT:
     break;
	 case FRUITS:
	 // nothing happens here.. sprite is loaded.
	 break;
   }
   ctx.fillRect(x*tw, y*th, tw, th);
  }
  
 }
 // changes the fillstyle once more and draws the score
 // message to the canvas
 ctx.fillStyle = "#000";
 ctx.fillText("SCORE: " + score, 70, canvas.height-10);
}

// start and run the game
menu();
