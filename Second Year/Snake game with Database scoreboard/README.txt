General information about the game and reused code and files.

# Snake Game 
I have used this base to do modifications to the game, all source and credit goes to Max Wihlborg.
maxwihlborg (2019). maxwihlborg/youtube-tutorials. [online] GitHub. 
Available at: https://github.com/maxwihlborg/youtube-tutorials/blob/master/snake/index.html 

Note: Files that are not given source link in section #Files Source are my own original work.
For further information about modification please open about tab when starting the game.

# Rules
Game has the following Rules:
	-Eating apples make your snake length longer 
	-Snake will die if it eats his own tail or colide in the 25x25 grid
	-Every apple is worth 5 points, but I spawn a cheating point which increment your score 1000 it is invisible.
	-You have to collect as many apples as possible to compete against the leaderboard

# Controls

Your keys are these to move the snake on the screen, ARROWUP,ARROWDOWN,ARROWLEFT,ARROWRIGHT 
each arrow is defined to move the snake.

# Code reuse

I have gathered general information for my project such as :

XMLHttpRequest - https://www.w3schools.com/xml/xml_http.asp

HtmlSpecialChars - https://www.php.net/manual/en/function.htmlspecialchars.php

General Snake Code - https://github.com/maxwihlborg/youtube-tutorials/blob/master/snake/index.html 

MIT permission - https://github.com/maxwihlborg/youtube-tutorials


# Files Source
eat.wav		- software used called bfxr open source code https://www.bfxr.net/
bgmusic.wav 	- background music gathered from youtube https://www.youtube.com/watch?v=tUcaD3yiqEY/
img/food.png	- apple sprite gathered from internet https://www.kissclipart.com/fruta-sprite-png-clipart-fruit-computer-icons-clip-7l0w39/

# Files in directory
Folder assets/css - css styling for the page
Folder img        - images for the game(snakeHead.png,food.png) 
eat.wav		  - snake's eating sound
bgmusic.wav 	  - background music for snake
about.html	  - general information about game and modification by me and every sources linked there.
index.html 	  - main page where the game.js is loaded.
add_to_leaderboard.php - adds (score,name) to postgres database.
read_data_from_leaderboard.php - reads data from postgres db to our page using xhr (score,name);
connection.php 	  - PostGresDB login information then included to both of the php file.
pdo.php	          - Test on PDO.


